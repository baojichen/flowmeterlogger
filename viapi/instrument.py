import time
import copy
import difflib

from .utils import *
from .privilege import *
from .register import *
from .db import DB
from .io import IO
from .memory import Memory

class Instrument:
    """instrument"""
    # self.__memory: Memory
    # self.__verbose: bool
    # self.__db: DB
    # self.__regs: dict [str -> Register]
    # self.__flags: int
    # self.__personality: Personality
    
    # This is the URL to the register database.
    # If set to None database is searched in local site-packages directory.
    # If not found there it is fetched from web ("https://api.voegtlin.com").
    REPOSITORY = None
    
    # Flags
    FLAG_NONE = 0
    FLAG_FORCE_LIST = 1
    
    # Create a new instrument connected via given communication channel
    def __init__(self, device = None, version = None, flags = None):
        self.__memory = None
        self.__verbose = False
        # Initially create an anonymous Personality
        self.__personality = Personality()
        self.__personality.register(self)
        # Bind device
        self.bind(device, version, flags)
    
    # Disconnect any serial channel when object is destroyed
    def __del__(self):
        self.disconnect()
    
    # Returns the device name as a string.
    @property
    def device(self):
        if self.__db is None:
            return None
        return self.__db.device
    
    # Returns the device version as a string.
    @property
    def version(self):
        if self.__db is None:
            return None
        return self.__db.version
    
    # Binds the specified device to this instrument.
    # Any previously bound device is discarded.
    # However user and IO channel is kept.
    def bind(self, device, version = None, flags = None):
        if (device is not None) and not isinstance(device, str):
            raise TypeError('Device: expecting a string type')
        if (version is not None) and not isinstance(version, str):
            raise TypeError('Version: expecting a string type')
        if (flags is not None) and not isinstance(flags, int):
            raise TypeError('Flags: expecing an integer value')
        if flags is not None:
            self.__flags = flags
        else:
            self.__flags = Instrument.FLAG_NONE
        # Load register definitions and create register objects.
        self.__db = DB(device, version, Instrument.REPOSITORY)
        # Upon object creation this has no effect because we have no IO channel yet.
        # However when called again after an IO channel was connected
        # we want to run auto-detection if no device was assigned.
        self.__auto_detect()
        # Mapping: string -> class Register
        self.__regs = self.__db.regs
    
    # Use given IO channel to connect this instrument.
    # If argument is set to None currently used IO channel is discarded.
    # Returns self to allow object allocation and connection using a single call:
    #    device = Instrument('xyz').connect(where)
    def connect(self, channel):
        if channel is not None:
            if not isinstance(channel, IO):
                raise TypeError('IO: expecting IO object')
            # Implicitly: Discard buffered memory sizes/base addresses when changing IO
            self.__memory = Memory(channel)
            self.verbose = self.__verbose
            # If no device is bound so far we try to auto-detect the device
            self.__auto_detect()
        else:
            self.disconnect()
        return self
    
    # Disconnects (discards) the current IO channel
    def disconnect(self):
        if self.__memory is not None:
            if self.__memory.channel is not None:
                self.__memory.channel.close()
        self.__memory = None
    
    # Clear internal cache
    def sync(self):
        if self.__memory is not None:
            self.__memory.reset()
    
    # Associates the specified personality or removes the current personality in use.
    # If given personality is None the current personality is removed.
    # In this case a new anonymous personality is created and used internally.
    def user(self, personality):
        if personality is not None:
            if not isinstance(personality, Personality):
                raise TypeError('Expecting a Personality type')
        else:
            personality = Personality()
        # If personality does not change we have nothing to do
        if self.__personality != personality:
            # If currently a different personality is in use
            # unregister this personality before replacing it
            if self.__personality is not None:
                self.__personality.unregister(self)
            # Replace personality
            self.__personality = personality
            self.__personality.register(self)
            # Login as current active user of the given personality
            self.login(personality.user, personality.password, personality)
    
    # Returns current personality object
    @property
    def personality(self):
        return self.__personality
    
    # Returns if 'verbose' mode is ON (True) or OFF (False).
    # While set to True underlying MODBUS channel emits debug information to standard output.
    @property
    def verbose(self):
        # If connected prefer actual minimalmodbus settings since user can bypass this function
        if self.__is_connected():
            result = self.__memory.channel.channel.debug
        else:
            result = self.__verbose
        return bool(result)
    
    # Sets 'verbose' mode ON (True) or OFF (False).
    # While set to True underlying MODBUS channel emits debug information to standard output.
    @verbose.setter
    def verbose(self, value):
        if not isinstance(value, bool):
            raise TypeError('Expecting a bool type')
        self.__verbose = bool(value)
        if self.__is_connected():
            self.__memory.channel.channel.debug = self.__verbose
    
    # Login as the specified user and the given password.
    # Typically this is not called directly but as
    # a callback from associated personality object.
    # It can be called directly by the user however.
    # In that case if personality is None and no personality object
    # is available yet a new personality object is created.
    def login(self, user, password = None, personality = None):
        if not isinstance(user, str):
            raise TypeError('User must be a string type')
        if password is not None:
            if not isinstance(password, int):
                raise TypeError('Password must be an integer type')
            if (password < 0) or (password > 0xFFFFFFFF):
                raise ValueError('Password must be in range: 0 <= password <= 0xFFFFFFFF')
        if personality is not None:
            if not isinstance(personality, Personality):
                raise TypeError('Expecting a Personality type')
        # If function is invoked as a callback we need to prevent endless loops:
        # Don't call any methods on personality object in that case.
        #
        # When invoked as a callback:
        # a) personality argument is never None
        # b) personality argument equals our personality member
        # c) self.__personality is not None because we have registered ourself for callback
        if self.__personality != personality:
            # Direct invocation of this function (no callback)
            # If we have no personality yet either create a new one or use the given argument and initialize it.
            if self.__personality is None:
                if personality is None:
                    self.__personality = Personality()
                else:
                    self.__personality = personality
                self.__personality.register(self)
            # At this point we know self.__personalitiy is never None
            self.__login_callback(user, password)
        else:
            # At this point we have a login callback
            if password is not None:
                try:
                    # Change current active user
                    self.__write_single('PASSWORD', password)
                except:
                    pass
    
    # Reads one or multiple registers from connected instrument.
    # If the argument is a string a single register is read.
    # Otherwise if the argument is a list multiple registers are read.
    # The list contains the registers to read as strings.
    # A list item can be a dictionary that also specifies count and offset.
    #
    # For example to read 2 elements from register XYZ at offset 1:
    # {
    #     'register': 'XYZ',
    #     'count': 2,
    #     'offset': 1
    # }
    def read(self, *args, **kwargs):
        # type(args)=tuple (if args not given this is an empty tuple)
        # type(kwargs)=dict (if kwargs not given this is an empty dict)
        # Tuple is immutable so convert args to list and append kwargs.
        args = list(args)
        if len(kwargs) > 0:
            args.append(kwargs)
        # Process read() request
        result = []
        for name in args:
            if not isinstance(name, list):
                result += self.__read_gateway(name)
            else:
                # If a list is given we issue a read request for each element.
                # Each element can be a string (the entire register is read) or a dictionary.
                for reg in name:
                    result += self.__read_gateway(reg)
        # If the result list contains a single element only
        # it is un-boxed and returned as a scalar value.
        # However this behavior can be turned off by flag.
        if (self.__flags & Instrument.FLAG_FORCE_LIST) == 0:
            if len(result) == 1:
                result = result[0]
        return result
    
    # Writes the given register to the connected instrument.
    def write(self, *args, **kwargs):
        # type(args)=tuple (if args not given this is an empty tuple)
        # type(kwargs)=dict (if kwargs not given this is an empty dict)
        # Tuple is immutable so convert args to list and append kwargs.
        args = list(args)
        if len(kwargs) > 0:
            args.append(kwargs)
        # Writing is a bit special:
        # In the argument list we first have the register addressing info
        # each one followed by the actual data to write.
        # When using kwargs we can't append data this way.
        # Instead we support a 'value' key in dictionary.
        # These value keys are extracted and appended/inserted into list.
        expand = []
        for value in args:
            expand.append(value)
            if isinstance(value, dict):
                if 'value' in value:
                    expand.append(value.pop('value'))
        args = expand
        # Process write() request
        result = 0
        if len(args) % 2 != 0:
            raise ValueError('Missing data for register')
        for name, data in zip(args[0::2], args[1::2]):
            if not isinstance(name, list):
                result += self.__write_gateway(name, data)
            else:
                # At this point we know that data is a list.
                # However the list can be flat or nested (a list of lists).
                if not isinstance(data, list):
                    raise ValueError('Register list given but data is scalar')
                if len(name) != len(data):
                    raise ValueError('Length of register list and data list must be equal')
                for reg, value in zip(name, data):
                    result += self.__write_gateway(reg, value)
        return result
    
    # Read the specified memory (given by zero-based index) and
    # store the data in the given file (as binary).
    # If count is given it specifies the number of bytes to read.
    # If count is set to zero the entire memory is read.
    # In this case the function determines the size of the memory to read internally.
    # The function returns the number of bytes successfully read.
    def read_memory(self, memory, file, count = 0, offset = 0):
        if not self.__is_connected():
            raise RuntimeError('No I/O channel connected')
        if not isinstance(memory, int):
            raise TypeError('Memory index must be an integer type')
        if (memory < 0) or (memory > 4):
            raise ValueError('Invalid memory index: {}'.format(memory))
        # Better exceptions in case the user has not sufficient privilege to read the memory.
        # Otherwise we get some kind of I/O exception because reading fails.
        self.__request_access(self.__regs['MEMORY_{}'.format(memory)], 'read')
        return self.__memory.read_memory(memory, file, count, offset)
    
    # Write the contents of the specified file into memory (given by zero-based index).
    # The number of bytes written is limited to specified count.
    # If count is omitted (or zero) the entire file is written
    # up to end of memory if the filesize exceeds the memory size.
    # An optional offset specifies the position in memory where to start writing.
    # The function returns the number of bytes actually written into memory.
    def write_memory(self, memory, file, count = 0, offset = 0):
        if not self.__is_connected():
            raise RuntimeError('No I/O channel connected')
        if not isinstance(memory, int):
            raise TypeError('Memory index must be an integer type')
        if (memory < 0) or (memory > 4):
            raise ValueError('Invalid memory index: {}'.format(memory))
        # Better exceptions in case the user has not sufficient privilege to write the memory.
        # Otherwise we get some kind of I/O exception because writing fails.
        self.__request_access(self.__regs['MEMORY_{}'.format(memory)], 'write')
        return self.__memory.write_memory(memory, file, count, offset)
    
    # Generates a delay (in ms).
    # The function returns when the given time has elapsed.
    # If given duration is zero or a negative value the function has no effect.
    def delay(self, ms):
        duration = float(ms)
        if duration > 0.0:
            # time.sleep() argument is in seconds
            time.sleep(duration / 1000.0)
    
    # Return a list of registers defined for the current device.
    # If there are no registers an empty list is returned.
    @property
    def registers(self):
        if self.__regs is not None:
            result = list(self.__regs.keys())
        else:
            result = []
        return result
    
    # Check and return if the specified register(s) exist.
    # Multiple registers can be given as arguments in which case
    # the function returns a list of bool values.
    # If a single register is given only the result is un-boxed to
    # a scalar value if Instrument.FLAG_FORCE_LIST flag is enabled.
    # Otherwise it is returned as a list containing a single value only.
    def exists(self, *args):
        result = []
        for name in args:
            if isinstance(name, str):
                result.append(name in self.__regs)
            else:
                if isinstance(name, (list, tuple, set)):
                    for p in name:
                        result.append(self.exists(p))
                else:
                    raise TypeError('Argument must be string or list type: {}'.format(name))
        # If the result list contains a single element only
        # it is un-boxed and returned as a scalar value.
        # However this behavior can be turned off by flag.
        if (self.__flags & Instrument.FLAG_FORCE_LIST) == 0:
            if len(result) == 1:
                result = result[0]
        return result
    
    # Return if the specified register(s) are readable for current active user.
    # If the specified register does not exist False is returned.
    # Multiple registers can be given as arguments in which case
    # the function returns a list of bool values.
    # If a single register is given only the result is un-boxed to
    # a scalar value if Instrument.FLAG_FORCE_LIST flag is enabled.
    # Otherwise it is returned as a list containing a single value only.
    def readable(self, *args):
        result = []
        for name in args:
            if isinstance(name, str):
                value = False
                if name in self.__regs:
                    group = self.__regs[name].access.users('read')
                    if group[0] != Personality.USERS[0]:
                        # Check if current active user is in list of
                        # users able to read this register
                        if (self.__personality is not None) and (self.__personality.user in group):
                            value = True
                    else:
                        # Assume that we can always do what user anonymous can do.
                        # We get to this point as well if self.__personality is None.
                        # Then we assume that we operate as anonymous.
                        value = True
                result.append(value)
            else:
                if isinstance(name, (list, tuple, set)):
                    for p in name:
                        result.append(self.readable(p))
                else:
                    raise TypeError('Argument must be string or list type: {}'.format(name))
        # If the result list contains a single element only
        # it is un-boxed and returned as a scalar value.
        # However this behavior can be turned off by flag.
        if (self.__flags & Instrument.FLAG_FORCE_LIST) == 0:
            if len(result) == 1:
                result = result[0]
        return result
    
    # Return if the specified register(s) are writable for current active user.
    # If the specified register does not exist False is returned.
    # Multiple registers can be given as arguments in which case
    # the function returns a list of bool values.
    # If a single register is given only the result is un-boxed to
    # a scalar value if Instrument.FLAG_FORCE_LIST flag is enabled.
    # Otherwise it is returned as a list containing a single value only.
    def writable(self, *args):
        result = []
        for name in args:
            if isinstance(name, str):
                value = False
                if name in self.__regs:
                    group = self.__regs[name].access.users('write')
                    if group[0] != Personality.USERS[0]:
                        # Check if current active user is in list of
                        # users able to write this register
                        if (self.__personality is not None) and (self.__personality.user in group):
                            value = True
                    else:
                        # Assume that we can always do what user anonymous can do.
                        # We get to this point as well if self.__personality is None.
                        # Then we assume that we operate as anonymous.
                        value = True
                result.append(value)
            else:
                if isinstance(name, (list, tuple, set)):
                    for p in name:
                        result.append(self.writable(p))
                else:
                    raise TypeError('Argument must be string or list type: {}'.format(name))
        # If the result list contains a single element only
        # it is un-boxed and returned as a scalar value.
        # However this behavior can be turned off by flag.
        if (self.__flags & Instrument.FLAG_FORCE_LIST) == 0:
            if len(result) == 1:
                result = result[0]
        return result
    
    # Return a dictionary of symbols that define the properties of the current device.
    # If there are no symbols an empty dictionary is returned.
    @property
    def symbols(self):
        result = {}
        if self.__db is not None:
            p = self.__db.preprocessor
            if p is not None:
                # Create a dictionary mapping keys and values
                keys = p.keys()
                values = p.get(keys)
                result = dict(zip(keys, values))
        return result
    
    # Returns a copy of the preprocessor in use
    # Mutating this copy does not mutate the preprocessor in use.
    @property
    def preprocessor(self):
        if self.__db is None:
            return None
        return copy.copy(self.__db.preprocessor)
    
    # Returns a copy of the register data base in use.
    @property
    def database(self):
        if self.__db is None:
            return None
        return copy.copy(self.__db)
    
    # Returns the raw I/O object in use
    # This is not a defensive copy!
    @property
    def raw(self):
        return self.__memory
    
    # Auto-detect unknown devices and bind to their register definitions.
    # This has no effect if connected device is not unknown or no IO channel is available.
    def __auto_detect(self):
        if (self.__memory is not None) and ((not hasattr(self.__db, 'device'))or(self.__db.device is None)):
            Logger.write('Try to auto-detect unknown device')
            # Key is actually the value of MEM_MIRRORS which is device specific.
            # We use this to identify the device.
            # After that fine selection of suitable register definitions
            # is done via hardware and software version.
            key = self.__memory.mirrors()
            if key > 0:
                software = self.__memory.version('software')
                hardware = self.__memory.version('hardware')
                Logger.write('Device key {}, hardware version {}, software version {}'.format(hex(key), hardware, software))
                device, version = self.__db.resolve(key, software)
            else:
                Logger.write('Try to auto-detect legacy device')
                device, version = self.__detect_legacy()
            Logger.write('Auto-detected device "{}" version "{}"'.format(device, version))
            self.bind(device, version, self.__flags)
    
    # Try to auto-detect legacy devices like Smart6 and Compact2.
    # The function returns tuple ("generic", None) is no legacy device has been detected.
    def __detect_legacy(self):
        try:
            # Hardware version for legacy devices is at MODBUS address 0x0020
            version = self.__memory.channel.read(0x0020, 1)[0]
            if version >= 0x600:
                return ('smart', '6.0.0')
            if version >= 0x300:
                return ('compact', '2.0.0')
        except:
            pass
        return ('generic', None)
    
    # Request specified access ('read' or 'write') for given register.
    # If unable to obtain that access the function raises an exception.
    def __request_access(self, register, access):
        group = register.access.users(access)
        if (group is None) or (len(group) <= 0):
            raise RuntimeError('No user is able to {} register {}'.format(access, register.name))
        # Assume that we can always do what user anonymous can do
        if group[0] != Personality.USERS[0]:
            if self.__personality is None:
                raise RuntimeError('Have no personality to gain required privilege to {} register {}'.format(access, register.name))
            if not self.__personality.user in group:
                if not self.__personality.request(group):
                    raise RuntimeError('Unable to gain privilege to {} register {}'.format(access, register.name))
    
    # Reads the requested amount of elements from specified register.
    # If the specified register does not exist an exception is thrown.
    # The elements are returned as a list even if a single value is requested only.
    # Requesting to read a zero amount of elements has no effect and returns an empty list.
    def __read_single(self, name, count = None, offset = None):
        if not self.__is_connected():
            raise RuntimeError('No I/O channel connected')
        if not name in self.__regs:
            raise ValueError('Unknown register "{}" {}'.format(name, self.__hint_matches(name)))
        reg = self.__regs.get(name)
        # Ensure read access to this register is granted
        # and change current active user if needed.
        self.__request_access(reg, 'read')
        # Process the request
        request = Array(reg.type.size(), reg.count, reg.element)
        result = []
        for blob in request.iterate(offset, count):
            # blob: [offset (bytes), number of elements, size of elements (in bytes)]
            # memory.read(address, offset_bytes, count_bytes)
            # To merge lists use + operator
            result += self.__memory.read(reg.address, offset = blob[0], count = blob[2])
        # Invalidate device register cache if needed.
        # This is typically the case if device is reset:
        # Then all volatile register data is lost.
        if reg.cache.invalidates('read'):
            self.__memory.reset()
        # Add a delay after read operation on this register
        self.delay(reg.delay['read'])
        # We always have a list of bytes here:
        # Do the type conversions
        return reg.type.unserialize(result)
    
    # Writes the specified data into the given register.
    # Register name must be a string.
    # The data to write can be a scalar or a list of values.
    # The type of those values must match the register type.
    # If the list contains more values than the underlying register holds
    # all items exceeding the area of the register are ignored.
    def __write_single(self, name, data, count = None, offset = None):
        if not self.__is_connected():
            raise RuntimeError('No I/O channel connected')
        if not name in self.__regs:
            raise ValueError('Unknown register "{}" {}'.format(name, self.__hint_matches(name)))
        # Put any scalar value into a list containing this element only.
        if not isinstance(data, list):
            data = [data]
        reg = self.__regs.get(name)
        # Ensure write access to this register is granted
        # and change current active user if needed.
        self.__request_access(reg, 'write')
        # Process the request
        result = 0
        request = Array(reg.type.size(), reg.count, reg.element)
        for blob in request.iterate(offset, count):
            # Combined types, e.g. type=["U32", "F32", "U16", "S16"] require special handling:
            # For each primitive type there's one value in data list,
            # but the register count refers to them as being one item.
            N = blob[1]*len(reg.type.type)
            # blob: [offset (bytes), number of elements, size of elements (in bytes)]
            buffer = reg.type.serialize(data[result:result+N])
            # write(address, offset, data)
            self.__memory.write(reg.address, offset = blob[0], data = buffer)
            # Number of elements (not bytes) processed
            result += N
        # Invalidate device register cache if needed.
        # This is typically the case if device is reset:
        # Then all volatile register data is lost.
        if reg.cache.invalidates('write'):
            self.__memory.reset()
        # Add a delay after write operation on this register
        self.delay(reg.delay['write'])
        # Return number of items actually written.
        # For compound types this is the number of
        # items multiplied by the number of compound elements.
        return result
    
    # Gateway to call self.__read_single() that handles dictionary arguments
    def __read_gateway(self, reg):
        if not isinstance(reg, dict):
            # Reading by name only reads the entire register.
            # Offset defaults to zero and count defaults to register count.
            result = self.__read_single(reg)
        else:
            # The item is dictionary.
            # We support the following keys:
            #     'register': 'name'
            #     'count': 42
            #     'offset': 0
            if not 'register' in reg:
                raise ValueError('Missing key "register" in given dictionary')
            name = reg['register']
            # Error checking count and offset is done within self.__read_single()
            if 'count' in reg:
                # This is the dict containing count/offset of request
                count = reg['count']
            else:
                count = None
            if 'offset' in reg:
                # This is the dict containing count/offset of request
                offset = reg['offset']
            else:
                offset = None
            result = self.__read_single(name, count, offset)
        return result
    
    # Gateway to call self.__write_single() that handles dictionary arguments
    def __write_gateway(self, reg, data):
        if not isinstance(reg, dict):
            result = self.__write_single(reg, data)
        else:
            if not 'register' in reg:
                raise ValueError('Missing key "register" in given directory')
            name = reg['register']
            # Error checking count and offset is done within self.__write_single()
            if 'count' in reg:
                # This is the dict containing count/offset of request
                count = reg['count']
            else:
                count = None
            if 'offset' in reg:
                # This is the dict containing count/offset of request
                offset = reg['offset']
            else:
                offset = None
            result = self.__write_single(name, data, count, offset)
        return result
    
    # Hint on possible matches for given name and register names available.
    # The hint is returned as a string.
    def __hint_matches(self, name, message = None):
        result = "(nothing similar found)"
        if name is not None:
            match = difflib.get_close_matches(str(name), self.registers, 1, 0.8)
            if (match is not None) and (len(match) > 0):
                match = match[0]
                if message is not None:
                    result = str(message).format(match)
                else:
                    result = '(did you mean "{}"?)'.format(match)
        return result
    
    # Trigger a login callback using underlying Personality object.
    # If no password is given password may be requested via input() function.
    def __login_callback(self, user, password = None):
        # Changing current active user by request() method will create a callback to login()
        if password is None:
            # If called without a password that means we should check our personality object first
            # and if that fails to obtain the users' credentials we ask for password.
            if not self.__personality.request(user, exact = True):
                self.__personality.authenticate(user)
                self.__personality.request(user)
        else:
            # Note that if password is None authenticate() asks for password.
            # As a result of requesting new current active user we may(!) receive a callback.
            # This is because if current active user doesn't change we don't receive a callback.
            # But in this case we have nothing to do either.
            self.__personality.authenticate(user, password)
            self.__personality.request(user)
    
    # Returns whether an IO channel is connected with the device.
    # True if an IO channel is connected, False otherwise.
    def __is_connected(self):
        # Also check if IO.__channel was closed (set to None) because can bypass our interface
        if self.__memory is None or self.__memory.channel.channel is None:
            return False
        return True
    
    # Support index syntax: Read/write register
    def __getitem__(self, name):
        # Subtile: Must target read() because this handles FLAG_FORCE_LIST
        return self.read(name)
    
    # Support index syntax: Read/write register
    def __setitem__(self, name, data):
        return self.write(name, data)
    
    # Create a readable string representation of this object
    def __str__(self):
        return stringify(
            'memory', self.__memory,
            'flags', self.__flags,
            'regs', self.__regs,
            'personality', self.__personality
        )
    
    # Support with statement
    def __enter__(self):
        return self
    
    # Support with statement
    def __exit__(self, exc_type, exc_val, exc_trace):
        self.disconnect()
    
    def __repr__(self):
        return self.__str__()
