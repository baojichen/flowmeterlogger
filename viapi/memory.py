from .utils import *
from .io import IO
from .layer import Layer

# 0x0000.0000--0x0000.FFFF: MODBUS register range (WORD addressing)
# 0x0001.0000--0x0FFF.FFFF: Unused (reserved)
# 0x1000.0000--0x1FFF.FFFF: Memory #0 (BYTE addressing)
# 0x2000.0000--0x2FFF.FFFF: Memory #1 (BYTE addressing)
# 0x3000.0000--0x3FFF.FFFF: Memory #2 (BYTE addressing)
# ...
class Memory:
    """memory abstraction layer"""
    # self.__io: IO
    # self.__memory: dict [of dict]
    # self.__page: int
    # self.__mirrors: int
    # self.__layer: Layer
    
    # Create a new memory object on top of given IO channel
    def __init__(self, channel):
        if not isinstance(channel, IO):
            raise TypeError('IO: expecting IO object')
        self.__io = channel
        self.__layer = Layer(channel)
        self.reset()
    
    # Close serial port when object is destroyed
    def __del__(self):
        if self.__io is not None:
            self.__io.close()
        self.__io = None
    
    # Return the IO channel used by this object.
    @property
    def channel(self):
        return self.__io
    
    # Returns the memory mirrors occupied by the current device.
    # This is a bitmask where each 1-bit indicates that the corresponding memory mirror is used.
    # If unable to retrieve this value the function returns None.
    # The result is cached internally.
    def mirrors(self):
        if self.__mirrors is None:
            # MEM_MIRRORS (memory layer: 0x02)
            self.__mirrors = self.__read_raw(0x7FF4, 2, 0x02)
        return self.__mirrors
    
    # Returns the software, hardware, mechanics and/or bootloader version (as string types).
    # Without arguments the function reads all versions and returns them as a dictionary.
    # If the argument is a scalar string it specifies the version type to return.
    # If the argument is a list it specifies the versions to return as a dictionary.
    # If unable to retrieve a version the None is returned.
    def version(self, which = None):
        # INFO_VERSION_SOFTWARE, INFO_VERSION_HARDWARE, INFO_VERSION_BOOTLOADER (main layer: 0x10)
        regs = {'hardware': 0x0412, 'software': 0x0410, 'bootloader': 0x0411, 'mechanics': 0x0413}
        if which is None:
            which = list(regs.keys())
        # Check if input ...
        if isinstance(which, (list, tuple, set)):
            # ... is a list type
            result = {}
            for name in which:
                if not isinstance(name, str):
                    raise TypeError('Must specify version type to read as a string type')
                if not name in regs:
                    raise ValueError('Bad or unknown version type "{}"'.format(name))
                # If unable to retrieve the version this function returns zero
                value = self.__read_raw(regs[name], 1, 0x10)
                if value > 0:
                    value = str(0xFF & (value >> 8)) + '.' + str(0xF & (value >> 4)) + '.' + str(value & 0xF)
                else:
                    value = None
                result[name] = value
        else:
            # ... is a scalar value
            if not isinstance(which, str):
                raise TypeError('Must specify version type to read as a string type')
            if which not in regs:
                raise ValueError('Bad or unknown version type "{}"'.format(which))
            # If unable to retrieve the version this function returns zero
            value = self.__read_raw(regs[which], 1, 0x10)
            result = None
            if value > 0:
                result = str(0xFF & (value >> 8)) + '.' + str(0xF & (value >> 4)) + '.' + str(value & 0xF)
        return result
    
    # Invoke this method if connected device has lost its volatile state.
    # This typically happens if the device is reset.
    # As a result this object discards any internally buffered state
    # and reloads these values from the device as soon as needed.
    # Otherwise this object will use outdated state.
    def reset(self):
        self.__page = None
        self.__mirrors = None
        self.__memory = {}
        self.__layer.reset()
        Logger.write('Invalidated local cache')
    
    # Read a block of memory from the specified address
    # having the given size (in bytes) and byte offset.
    # Whether its a byte or word address depends on the underlying memory.
    # The result is always returned as a list of bytes.
    def read(self, address, offset, count):
        if (address < 0) or (address > 0xFFFFFFFF):
            raise ValueError('Invalid address: Must be 0 <= address <= 0xFFFFFFFF')
        if offset < 0:
            raise ValueError('Offset must be positive or zero')
        if count < 0:
            raise ValueError('Unable to read a negative quantity of bytes')
        result = []
        if count > 0:
            # Note that we do not support crossing memory boundaries.
            # This is more a theoretical limitation because
            # the size of each memory map is huge and
            # much larger than any physical memory.
            device = self.__get_device(address)
            if device == 0:
                address += offset >> 1
                # Read targets MODBUS register range.
                # Read registers and split result into bytes:
                #           MSB[0] LSB[0] MSB[1] LSB[1] MSB[2] LSB[2]    (MODBUS is MSB first)
                # Addr      |---  0  ---| |---  1  ---| |---  2  ---|
                values = []
                for word in self.__io.read(address, (count + 1) >> 1):
                    values.append(0xFF & (word >> 8))
                    values.append(0xFF & word)
                # Support unaligned access here:
                # - Adjust for unaligned offsets: Cut away leading MSB
                # - Adjust for unaligned byte count: Cut away LSB of last word
                for index in range(offset & 1, count):
                    result.append(values[index])
            else:
                # Read targets device memory using a MODBUS tunnel.
                # In that case we've received a byte address.
                # We need to subtract the starting address of the device memory
                # so that we get an address that basically is an offset within that memory.
                address = self.__get_effective_addr(address, offset)
                # Read base address and size of memory.
                size = self.__get_size(device)
                if address + count > size:
                    raise RuntimeError('Read request spans into empty region of memory map')
                # Read memory
                result = self.__read_external_gateway(device, address, count)
        return result
    
    # Writes a block of memory starting at the specified address using the byte offset.
    # The data is passed as a list of bytes.
    def write(self, address, offset, data):
        if (address < 0) or (address > 0xFFFFFFFF):
            raise ValueError('Invalid address: Must be 0 <= address <= 0xFFFFFFFF')
        if offset < 0:
            raise ValueError('Offset must be positive or zero')
        count = len(data)
        if count > 0:
            # Note that we do not support crossing memory boundaries.
            # This is more a theoretical limitation because
            # the size of each memory map is huge and
            # much larger than any physical memory.
            device = self.__get_device(address)
            if device == 0:
                # Write targets MODBUS register range.
                # Support unaligned access:
                # - Adjust for unaligned offsets: Read leading MSB, combine with LSB/data
                # - Adjust for unaligned byte count: Read trailing LSB, combine with MSB/data
                #           MSB[0] LSB[0] MSB[1] LSB[1] MSB[2] LSB[2]
                # Addr      |---  0  ---| |---  1  ---| |---  2  ---|
                #                 |
                # Offset (=1) ----+
                # Count           |<------  3  ------>|
                if (offset & 1) != 0:
                    try:
                        word = self.__io.read(address + (offset >> 1), 1)[0]
                    except:
                        word = 0
                    data.insert(0, 0xFF & (word >> 8))
                lastbyte = offset + count
                if lastbyte & 1 != 0:
                    try:
                        # Some registers require read requests targeting their base address.
                        # Reading such a register here will fail.
                        word = self.__io.read(address + (lastbyte >> 1), 1)[0]
                    except:
                        word = 0
                    data.append(0xFF & word)
                # We receive a list of bytes as input but need to create a list of words.
                # Since we read and added leading MSB and trailing LSB
                # we can ignore any unaligned part of the address.
                output = []
                for index in range(0, len(data), 2):
                    word = (data[index] << 8) | data[index+1]
                    output.append(word)
                self.__io.write(address + (offset >> 1), output)
            else:
                address = self.__get_effective_addr(address, offset)
                # Read base address and size of memory
                size = self.__get_size(device)
                if address + count > size:
                    raise RuntimeError('Write request spans into empty region of memory map')
                # Write memory
                self.__write_external_gateway(device, address, data)
    
    # Read the specified memory (given by zero-based index) and
    # store the data in the given file (as binary).
    # If count is given it specifies the number of bytes to read.
    # If count is set to zero the entire memory is read.
    # In this case the function determines the size of the memory to read internally.
    # The function returns the number of bytes successfully read.
    def read_memory(self, memory, file, count = 0, offset = 0):
        if not isinstance(memory, int):
            raise TypeError('Memory index must be an integer type')
        # There's a maximum of 4 memory areas supported
        if (memory > 3) or (memory < 0):
            raise ValueError('Invalid memory index: {}'.format(memory))
        if not isinstance(file, str):
            raise TypeError('File name must be a string type')
        if not isinstance(count, int):
            raise TypeError('Number of bytes to read must be an integer type')
        if not isinstance(offset, int):
            raise TypeError('Memory offset must be an integer type')
        if offset < 0:
            raise ValueError('Invalid memory offset: {}'.format(offset))
        # Transform index: zero based => one based
        device = memory + 1
        if self.__get_readable(device) is False:
            raise RuntimeError('Specified memory #{} is not readable or does not exist'.format(memory))
        # Get number of bytes to read
        size = self.__get_size(device)
        if offset >= size:
            raise RuntimeError('Offset {} is exceeding size of memory #{} ({} bytes)'.format(hex(offset), memory, hex(size)))
        if count <= 0:
            count = size
        # Note that offset+count can still exceed the memory size.
        # Limit count to read everything up to end of memory
        if offset + count > size:
            count = size - offset
        # Read memory copying chunks to given file
        BLOCK_SIZE = 64
        result = 0
        with open(file, "wb") as target:
            last = offset + count
            while offset < last:
                # Get number of bytes to read in next chunk
                N = min(BLOCK_SIZE, last - offset)
                # Read block from external memory.
                # If reading fails retry before raising an exception
                retry = 3
                while retry >= 0:
                    try:
                        data = self.__read_external_gateway(device, offset, N)
                        retry = -1
                    except:
                        if retry <= 0:
                            raise RuntimeError('Unable to read {} bytes from memory #{} at offset {}'.format(N, memory, hex(offset)))
                        retry = retry - 1
                # Convert to binary data
                buffer = b""
                for i in range(0, len(data)):
                    buffer += bytes([data[i] & 0xFF])
                # Save to file
                target.write(buffer)
                target.flush()
                # Abort if there was a problem reading the memory
                result += len(data)
                if len(data) != N:
                    break
                offset += BLOCK_SIZE
        return result
    
    # Write the contents of the specified file into memory (given by zero-based index).
    # The number of bytes written is limited to specified count.
    # If count is omitted (or zero) the entire file is written
    # up to end of memory if the filesize exceeds the memory size.
    # An optional offset specifies the position in memory where to start writing.
    # The function returns the number of bytes actually written into memory.
    def write_memory(self, memory, file, count = 0, offset = 0):
        if not isinstance(memory, int):
            raise TypeError('Memory index must be an integer type')
        # There's a maximum of 4 memory areas supported
        if (memory > 3) or (memory < 0):
            raise ValueError('Invalid memory index: {}'.format(memory))
        if not isinstance(file, str):
            raise TypeError('File name must be a string type')
        if not isinstance(count, int):
            raise TypeError('Number of bytes to read must be an integer type')
        if not isinstance(offset, int):
            raise TypeError('Memory offset must be an integer type')
        if offset < 0:
            raise ValueError('Invalid memory offset: {}'.format(offset))
        # Transform index: zero based => one based
        device = memory + 1
        if self.__get_writable(device) is False:
            raise RuntimeError('Specified memory #{} is not writable or does not exist'.format(memory))
        # Get number of bytes to write
        size = self.__get_size(device)
        if offset >= size:
            raise RuntimeError('Offset {} is exceeding size of memory #{} ({} bytes)'.format(hex(offset), memory, hex(size)))
        if count <= 0:
            count = size
        # Note that offset+count can still exceed the memory size.
        # Limit count to read everything up to end of memory
        if offset + count > size:
            count = size - offset
        # Write memory copying chunks from given file
        BLOCK_SIZE = 64
        result = 0
        with open(file, "rb") as source:
            last = offset + count
            while offset < last:
                # Get number of bytes to write in next chunk
                N = min(BLOCK_SIZE, last - offset)
                # Read next chunk from file
                data = source.read(N)
                # Convert bytes() to list of bytes
                # This returns bytes() object for binary mode file,
                # which is an array of bytes.
                # Indexing this array returns bytes as integer values.
                buffer = []
                for i in range(0, len(data)):
                    buffer.append(data[i])
                # Write to external device
                written = 0
                retry = 3
                while retry >= 0:
                    try:
                        written = self.__write_external_gateway(device, offset, buffer)
                        retry = -1
                    except:
                        if retry <= 0:
                            raise RuntimeError('Unable to write {} bytes to memory #{} at offset {}'.format(len(buffer), memory, hex(offset)))
                        retry = retry - 1
                # Check if we've reached EOF or there was a problem writing memory.
                # In both cases we can stop writing and abort.
                result += written
                if written != N:
                    break
                offset += BLOCK_SIZE
        return result
    
    # Reads a list of bytes from given address in specified external device.
    # The result is returned as a list of bytes (even if only a single byte was requested).
    # Address is the zero based offset within the external device memory.
    # Clears internal cache and retry on MODBUS exception.
    def __read_external_gateway(self, device, address, count = 1):
        result = []
        try:
            result = self.__read_external(device, address, count)
        except:
            # Did device reset without our knowledge? clear cache!
            self.reset()
            result = self.__read_external(device, address, count)
        return result
    
    # Reads a list of bytes from given address in specified external device.
    # The result is returned as a list of bytes (even if only a single byte was requested).
    # Address is the zero based offset within the external device memory.
    def __read_external(self, device, address, count = 1):
        result = []
        if count > 0:
            base = self.__get_base(device)
            index = 0
            while index < count:
                self.__set_page(base + ((address + index) >> 12))
                page_offset = (address + index) & 0xFFF
                # We can read up to end of page bytes without changing page
                # but a smaller amount of bytes may have been requested.
                N = min(0x1000 - page_offset, count - index)
                # Read and remove MSBs (safety only: they should be zero)
                result += [word & 0xFF for word in self.__io.read(0x8000 + page_offset, N)]
                index += N
        return result
    
    # Writes a list of bytes to the given address in specified external device.
    # Returns the number of bytes actually written.
    # Clears internal cache and retry on MODBUS exception.
    def __write_external_gateway(self, device, address, data):
        result = 0
        try:
            result = self.__write_external(device, address, data)
        except:
            # Did device reset without our knowledge? clear cache!
            self.reset()
            result = self.__write_external(device, address, data)
        return result
    
    # Writes a list of bytes to the given address in specified external device.
    # Returns the number of bytes actually written.
    def __write_external(self, device, address, data):
        index = 0
        if data is not None:
            if not isinstance(data, list):
                data = [data]
            count = len(data)
            if count > 0:
                base = self.__get_base(device)
                while index < count:
                    self.__set_page(base + ((address + index) >> 12))
                    page_offset = (address + index) & 0xFFF
                    # We can write up to end of page bytes without changing page
                    # but a smaller amount of bytes may have been requested.
                    N = min(0x1000 - page_offset, count - index)
                    self.__io.write(0x8000 + page_offset, data[index:(index + N)])
                    index += N
        return index
    
    # Read the specified number of registers starting at given MODBUS address.
    # The value is returned as an integer (MSB first).
    def __read_raw(self, addr, count, layer = None):
        result = 0
        # Try to read without changing user privilege first.
        # Only if that fails request manufacturer privilege and retry.
        value = []
        try:
            value = self.__io.read(addr, count)
        except:
            try:
                # LAYER_ENABLE
                # Does not throw and is a no-op internally if layer is None
                self.__layer.enable(layer)
                # PASSWORD
                self.__io.write(0xFFF8, [0x1234, 0x5678])
                value = self.__io.read(addr, count)
            except:
                pass
        result = 0
        for i in range(0, len(value)):
            result = (result << 16) | value[i]
        return result
    
    # Returns the memory device pointed to by given address.
    # This is a zero-based index with 0 referring to the standard MODBUS register memory map.
    # Any non-zero value refers to a memory device within the device.
    def __get_device(self, address):
        return (address & 0xF0000000) >> 28
    
    # Returns the effective address within device memory.
    def __get_effective_addr(self, address, offset):
        return (address - 0x10000000 * self.__get_device(address)) + offset
    
    # Sets the base address register.
    # Calling this function sets the value of MEM_BASE_ADDR register.
    def __set_page(self, page):
        # Only write base address if it deviates from current value.
        # Since self.__page is initially None it is always written on first call.
        # Be aware that caching this does not work as expected when device is reset.
        # This will also reset base address and at this point we may then
        # assume falsely that base address still has previous cached value.
        # To prevent that problem the user must call reset() method.
        if self.__page != page:
            self.__io.write(0x7FF0, [page >> 16, page & 0xFFFF])
            self.__page = page
    
    # Read the starting address of the memory associated with given device.
    # The given device index is one based (1 = first memory, 2 = second memory, and so on).
    # This function returns the value of MEM_n_ADDR register.
    def __get_base(self, device):
        result = 0
        if device > 0:
            self.__read_memory_info(device)
            result = self.__memory[device]['base']
        return result
    
    # Read the size of the memory associated with given device (in bytes).
    # The given device index is one based (1 = first memory, 2 = second memory, and so on).
    # This function returns the value of MEM_n_SIZE register.
    def __get_size(self, device):
        result = 0
        if device > 0:
            self.__read_memory_info(device)
            result = self.__memory[device]['size']
        return result
    
    # Returns whether the specified device memory (given by one based index) is readable:
    # True if readable, False otherwise.
    # If device index is zero the function always returns True.
    # If the device is not available False is returned.
    def __get_readable(self, device):
        result = True
        if device > 0:
            # MEM_FLAGS register value may change depending on the user logged in:
            # Therefore we don't buffer this register value.
            readable = self.__io.read(0x7FF3, 1)[0]
            # bit index          2  1  0
            #               ...|WR|WR|WR
            if ((readable >> ((device - 1) * 2)) & 1) == 0:
                result = False
        return result
    
    # Returns whether the specified device memory (given by one based index) is writable:
    # True if writable, False otherwise.
    # If device index is zero the function always returns True.
    # If the device is not available False is returned.
    def __get_writable(self, device):
        result = True
        if device > 0:
            # MEM_FLAGS register value may change depending on the user logged in:
            # Therefore we don't buffer this register value.
            writable = self.__io.read(0x7FF3, 1)[0]
            # bit index          2  1  0
            #               ...|WR|WR|WR
            if ((writable >> ((device - 1) * 2)) & 2) == 0:
                result = False
        return result
    
    # Read size and base address register
    def __read_memory_info(self, device):
        if device > 0:
            # Check if we already know the base address.
            # Only if we don't know the base address yet we read it via MODBUS.
            if not device in self.__memory:
                data = self.__io.read(0x7FE0 + (device - 1) * 4, 4)
                self.__memory[device] = {
                    'size': (data[0] << 16) | data[1],
                    'base': (data[2] << 16) | data[3]
                }
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'io', self.__io,
            'memory', self.__memory,
            'page', self.__page
        )
    
    def __repr__(self):
        return self.__str__()
