import os
import io
import zipfile

# io.IOBase -> io.RawIOBase -> io.FileIO
class fsobj(io.FileIO):
    """file system object"""
    
    def __init__(self, path, handle = None, mode = "r"):
        # Ensure handle is always a list (possibly empty)
        if handle is not None:
            if not isinstance(handle, list):
                handle = [handle]
        else:
            handle = []
        if path is not None:
            if not isinstance(path, str):
                path = str(path)
        else:
            path = ""
        if len(handle) > 0:
            # This is some object within a zip or an archive file
            # It seems that zipfile functions only can handle unix slashes
            path = path.replace("\\", "/")
            # If seems "rb" is not supported for zipfile, must be r,w,x or a
            handle.append(handle[-1].open(path, "r"))
        else:
            # Assume a pure file system path here
            handle = [open(path, mode)]
        self.__mode = str(mode).lower()
        self.__handle = handle
    
    # io.IOBase.close()
    # zipfile.ZipExtFile.close()
    def close(self):
        if self.__handle is not None:
            while len(self.__handle) > 0:
                self.__handle.pop().close()
            self.__handle = None
    
    # io.IOBase.closed
    @property
    def closed(self):
        return self.__handle is None
    
    # io.IOBase.fileno()
    def fileno(self):
        result = None
        handle = self.__get_handle()
        if handle is not None:
            if not isinstance(handle, zipfile.ZipExtFile):
                result = handle.fileno()
            else:
                # Assume zipfile.ZipExtFile can't have numeric descriptor
                raise OSError("object is within an archive")
        return result
    
    # io.IOBase.flush()
    def flush(self):
        # It seems that zipfile.ZipExtFile.flush() only inherits from io.IOBase
        handle = self.__get_handle()
        if handle is not None:
            handle.flush()
    
    # io.IOBase.isatty()
    def isatty(self):
        result = False
        handle = self.__get_handle()
        if handle is not None:
            # An archive is never a terminal, for any file object we have to query it.
            # It seems that zipfile.ZipExtFile.isatty() only inherits from io.IOBase
            if not isinstance(handle, zipfile.ZipExtFile):
                result = handle.isatty()
        return result
    
    # io.IOBase.readable()
    # zipfile.ZipExtFile.readable()
    def readable(self):
        result = False
        handle = self.__get_handle()
        if handle is not None:
            result = handle.readable()
        return result
    
    # io.IOBase.readline()
    # zipfile.ZipFileExt.readline()
    def readline(self, size = -1):
        result = bytes()
        handle = self.__get_handle()
        if handle is not None:
            result = handle.readline(size)
        return self.__adjust_type(result)
    
    # io.IOBase.readlines()
    # zipfile.ZipFileExt.readlines()
    def readlines(self, hint = -1):
        result = []
        handle = self.__get_handle()
        if handle is not None:
            result = handle.readlines(hint)
        return self.__adjust_type(result)
    
    # io.RawIOBase.read()
    # zipfile.ZipFileExt.read()
    def read(self, size = -1):
        result = bytes()
        handle = self.__get_handle()
        if handle is not None:
            result = handle.read(size)
        return self.__adjust_type(result)
    
    # io.RawIOBase.readall()
    def readall(self):
        # Type conversion already handled within read()
        return self.read()
    
    # io.RawIOBase.readinto()
    def readinto(self, buffer):
        if not isinstance(buffer, memoryview):
            buffer = memoryview(buffer)
        data = self.read(len(buffer))
        size = min(len(data), len(buffer))
        if isinstance(data, bytes):
            for i in range(0, size):
                buffer[i] = data[i]
        else:
            for i in range(0, size):
                buffer[i] = ord(data[i])
        return size
    
    # io.IOBase.seek()
    # zipfile.ZipExtFile.seek()
    def seek(self, offset, whence = io.SEEK_SET):
        result = 0
        handle = self.__get_handle()
        if handle is not None:
            result = handle.seek(offset, whence)
        return result
    
    # io.IOBase.seekable()
    # zipfile.ZipExtFile.seekable()
    def seekable(self):
        result = False
        handle = self.__get_handle()
        if handle is not None:
            result = handle.seekable()
        return result
    
    # io.IOBase.tell()
    # zipfile.ZipExtFile.tell()
    def tell(self):
        result = 0
        handle = self.__get_handle()
        if handle is not None:
            result = handle.tell()
        return result
    
    # io.FileIO.mode
    @property
    def mode(self):
        result = None
        handle = self.__get_handle()
        if handle is not None:
            result = self.__mode
        return result
    
    # io.FileIO.name
    @property
    def name(self):
        result = None
        handle = self.__get_handle()
        if handle is not None:
            result = handle.name
        return result
    
    # io.IOBase.writable()
    def writable(self):
        # Writing currently not supported
        return False
    
    # io.IOBase.truncate()
    def truncate(self, size = None):
        # Writing currently not supported
        raise io.UnsupportedOperation("Writing unsupported")
    
    # io.RawIOBase.write()
    def write(self, buffer):
        # Writing currently not supported
        raise io.UnsupportedOperation("Writing unsupported")
    
    # io.IOBase.writelines()
    def writelines(self, lines):
        # Writing currently not supported
        raise io.UnsupportedOperation("Writing unsupported")
    
    # Returns the internal handle to archive or file object
    def __get_handle(self):
        result = None
        if self.__handle is not None:
            if len(self.__handle) > 0:
                result = self.__handle[-1]
        else:
            raise ValueError("file is closed")
        return result
    
    # Reading from binary mode file should return bytes
    # reading from text mode file should return str
    def __adjust_type(self, data):
        if isinstance(data, list):
            for i in range(0, len(data)):
                data[i] = self.__adjust_type(data[i])
        else:
            # Hopefully some scalar type
            handle = self.__get_handle()
            if handle is not None:
                if self.mode.find("b") >= 0:
                    # Binary mode => bytes
                    if isinstance(data, str):
                        data = bytes([ord(c) for c in data])
                else:
                    # Text mode => str
                    if isinstance(data, bytes):
                        data = "".join([chr(c) for c in data])
        return data
    
    # io.IOBase.__del__()
    # Finalizer simply closes file handles and discard their references
    def __del__(self):
        self.close()
    
    def __iter__(self):
        return self
    
    def __next__(self):
        # Returns a single line from file
        result = self.readline()
        if (result is None) or (len(result) <= 0):
            raise StopIteration()
        return result
    
    def __enter__(self):
        return self
    
    def __exit__(self, exc_type, exc_val, exc_trace):
        self.close()
    
    # Create a readable string representation
    def __str__(self):
        if self.closed:
            return ""
        return self.name + " [" + self.mode + "]"
    
    def __repr__(self):
        return self.__str__()
