import os
import urllib

from .loader import Loader
from .os_utils import *

class Downloader:
    """url downloader"""
    PACKAGE = "viapi"
    TARGET = None
    
    # Download the resource pointed to by URL and stores it into a local file.
    # If no target file is specified the filename part of the URL is copied
    # and the file is placed in site-package directory of current library.
    # Returns the path of new file or None if download has failed.
    @staticmethod
    def get(url, target = None, base = None):
        if target is None:
            if base is None:
                # https://www.voegtlin.com/abc/xyz.html -> path = /abc/xyz.html
                path = urllib.parse.urlparse(url).path.lstrip("\\/")
            else:
                # base = https://www.voegtlin.com/abc/
                # url  = https://www.voegtlin.com/abc/xyz.html
                # relative directory -> xyz.html
                path = Downloader.__get_relative_dir(base, url)
            # If there is no path compound in URL assume it's index.html
            if len(path) > 0:
                target = os.path.join(Downloader.TARGET, path)
            else:
                target = os.path.join(Downloader.TARGET, "index.html")
        data = Loader.request(url)
        if data is not None:
            # If specified target is an existing directory
            # then store download as a new file within that directory.
            if os.path.isdir(target):
                # Inherit the name of that file from the URL.
                if base is None:
                    relative_part = os.path.split(url)[1]
                else:
                    relative_part = Downloader.__get_relative_dir(base, url)
                target = os.path.join(target, relative_part)
            # Store data in file
            target = Downloader.__store_file(target, data)
        else:
            target = None
        return target
    
    # Returns the default location where downloaded files are stored.
    @staticmethod
    def get_target_directory():
        return Downloader.TARGET
    
    # Sets the default location where downloaded files are stored.
    # If specified path is None this defaults to the package location e.g. within lib/site-packages.
    # If the specified target directory does not exist the function creates it.
    @staticmethod
    def set_target_directory(path):
        if path is not None:
            # Ensure that specified directory exists
            path = os.path.abspath(path)
            if not os.path.exists(path):
                os.makedirs(path)
            Downloader.TARGET = path
        else:
            Downloader.TARGET = os_utils.get_site_package_dir(Downloader.PACKAGE)
    
    # Returns the relative URL part.
    # Leading path separators are removed.
    @staticmethod
    def __get_relative_dir(base, url):
        if base is not None:
            if url.lower().startswith(base.lower()):
                url = url[len(base):].lstrip("\\/")
        return url
    
    # Store data in file where data can by of type "bytes" or "str".
    # Parent directories of file are created if not existing yet.
    # Returns the fully qualified name of the file created.
    @staticmethod
    def __store_file(name, data):
        name = os.path.abspath(name)
        # At this point we assume target includes a filename part.
        # We ensure that target directory part exists.
        dir_part = os.path.dirname(name)
        if not os.path.exists(dir_part):
            os.makedirs(dir_part)
        # Convert data into binary and write that to output file
        if not isinstance(data, bytes):
            # If data is not a string try to convert it into a string
            if not isinstance(data, str):
                data = str(data)
            binary = bytes([ord(data[i]) for i in range(0, len(data))])
        else:
            binary = data
        with open(name, "wb") as file:
            file.write(binary)
        return name

Downloader.set_target_directory(None)
