import sys
import re

from .http_cache import *

class http_resource:
    """Python 2/3 compatibility layer for HTTP requests"""
    # self.__data: str
    # self.__url: str
    # self.__gateway: function
    
    # urllib3 only: Connection pool manager
    __POOL = None
    
    # Create a new object optionally loading the contents of given URL.
    def __init__(self, url = None):
        self.__gateway = None
        try:
            if sys.version_info < (3, 0):
                # Python 2.x: Try to use urllib2
                import urllib2
                self.__gateway = self.__request_urllib2
            else:
                # Python 3.x: Try to use urllib3
                import urllib3
                self.__POOL = urllib3.PoolManager()
                self.__gateway = self.__request_urllib3
        except:
            # Fallback if either urllib2 or urllib3 doesn't work
            import urllib.request
            self.__gateway = self.__request_urllib
        # If an URL was given try to load it now
        if url is not None:
            self.request(url)
        else:
            self.__data = None
            self.__url = None
    
    # Requests the contents of the specified URL returning data as a string.
    # The specified URL must be a HTTP resource.
    def request(self, url):
        if (url is None) or not isinstance(url, str):
            raise TypeError('URL: expecting a string type')
        # Check if data is cached before requesting it via HTTP
        if http_cache.has(url) is False:
            # At this point data is not cached yet...
            if self.__gateway is None:
                raise RuntimeError('Unable to find any library to access URL')
            data = self.__gateway(url)
            # ...but we put it into the cache after download
            http_cache.update(url, data)
        else:
            # Data is already cache we simply reuse it
            data = http_cache.get(url)
        self.__data = data
        self.__url = url
        return self.__data
    
    # Returns the data (as a string) of the URL represented by this object.
    # If this object has not loaded a URL yet the function returns None.
    @property
    def data(self):
        return self.__data
    
    # Returns the URL represented by this object (as a string).
    # If this object has not loaded a URL yet the function returns None.
    @property
    def url(self):
        return self.__url
    
    # URL request implementation using urllib
    def __request_urllib(self, url):
        try:
            import urllib.request
            request = urllib.request.urlopen(url, timeout = 4.0)
        except:
            raise RuntimeError('I/O error while accessing resource: {}'.format(url))
        try:
            response = request.read()
        except:
            request.close()
            raise RuntimeError('I/O error while accessing resource: {}'.format(url))
        request.close()
        return response
    
    # URL request implementation using urllib2
    def __request_urllib2(self, url):
        # Python 2.x
        try:
            import urllib2
            # This raises an exception if url is not available
            url = urllib2.urlopen(url, timeout = 4.0)
        except:
            raise RuntimeError('I/O error while accessing resource: {}'.format(url))
        return url.read()
    
    # URL request implementation using urllib3
    def __request_urllib3(self, url):
        # Python 3.x
        try:
            import urllib3
            request = self.__POOL.request('GET', url, retries = 3, redirect = 3, timeout = 4.0)
        except:
            raise RuntimeError('I/O error while accessing resource: {}'.format(url))
        # HTTP OK: 200
        if request.status != 200:
            raise RuntimeError('Resource not found: {} (HTTP status {})'.format(url, request.status))
        # Transform response into a string using the charset given in HTTP response header
        charset = 'iso-8859-1'
        match = re.search(r'charset\s*=\s*([^;\s]+)', request.headers.get('content-type'), flags = re.I)
        if match is not None:
            charset = match.group(1)
        # request.data: class bytes
        response = request.data.decode(charset)
        request.close()
        return response
    
    # Create a readable string representation of this object
    def __str__(self):
        if self.__data is None:
            return ''
        return self.__data
    
    def __repr__(self):
        return self.__str__()
