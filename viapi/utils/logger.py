import io
import sys
import time

class Logger:
    """event logger"""
    
    # Channel to write message to
    __CHANNEL = sys.stdout
    
    # Log given message optionally appending given data.
    # This function never throws exceptions.
    @staticmethod
    def write(message, data = None):
        # If channel is None that means /dev/null
        if Logger.__CHANNEL is not None:
            try:
                value = time.strftime('%d.%m.%Y %H:%M:%S', time.localtime())
                if message is not None:
                    value += ': ' + str(message)
                if data is not None:
                    value += ' = '
                    if isinstance(data, (list, tuple, set)):
                        for index in range(0, len(data)):
                            if isinstance(data[index], int):
                                value += hex(data[index])
                            else:
                                value += str(data[index])
                            if index < len(data)-1:
                                value += ', '
                    else:
                        value += str(data)
                print(value, file = Logger.__CHANNEL, flush = True)
            finally:
                # Never break code by exceptions thrown by logger
                pass
    
    # Redirects the logger output to the given channel.
    # By default the output is sent to sys.stdout.
    # A previous assigned channel is closed when invoked with a new channel.
    @staticmethod
    def redirect(channel):
        if channel is not None:
            if isinstance(channel, str):
                channel = open(str(channel), 'a')
            if not isinstance(channel, io.IOBase):
                raise TypeError('Channel must be a file type or None')
        # If switching to a different channel close the old one first
        if channel != Logger.__CHANNEL:
            try:
                if isinstance(Logger.__CHANNEL, io.IOBase) and Logger.__CHANNEL not in [None, sys.stdout, sys.stderr]:
                    Logger.__CHANNEL.close()
            except:
                pass
        Logger.__CHANNEL = channel
