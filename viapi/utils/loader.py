import re
import os
import tempfile
import ftplib
import urllib3

from .logger import Logger
from .http_resource import http_resource
from .fs import *

class Loader:
    """url loader"""
    
    __HTTP = http_resource()
    
    @staticmethod
    def request(url):
        if (url is None) or not isinstance(url, str):
            raise TypeError('URL: expecting a string type')
        SCHEMA = {
            'file': Loader.__request_file,
            'http': Loader.__request_http,
            'https': Loader.__request_http,
            'ftp': Loader.__request_ftp
        }
        match = re.match(r'([a-z][a-z]+):', url, re.I)
        if match is not None:
            schema = match.group(1).lower()
        else:
            schema = 'file'
        if schema not in SCHEMA:
            raise ValueError('Schema "{}" not supported'.format(schema))
        Logger.write('{}: {}'.format(schema.upper(), url))
        return SCHEMA[schema](url)
    
    @staticmethod
    def __request_file(url):
        path = re.match(r'file:(.+)', url, re.I)
        if path is not None:
            # This may point to a network share or localhost.
            # If pointing to localhost we transform URL into a local path.
            match = re.match(r'file:[/\\]{2}(localhost)?[/\\](.+)', url, re.I)
            if match is not None:
                # Pointing to localhost.
                # file:///directory/file
                # file://localhost/directory/file
                url = match.group(2)
                # On Windows the URL contains the drive e.g.
                # file:///C:/directory/file
                # On Unix/Linux there is no drive and the URL denotes the path as is:
                # file://localhost/drive/directory/file
                # However on Unix/Linux this is an absolute path and must be prefixed with slash.
                if not os.name.lower().startswith('nt'):
                    url = '/' + url
                # This substitutes "." and ".." and handle slash/backslash translation
                url = os.path.realpath(url)
            else:
                # Most likely pointing to some network share.
                # Seem to be okay for Windows to keep slashes
                # file://server/directory/file
                url = path.group(1)
        else:
            # Just a plain old simple local file.
            url = os.path.realpath(url)
        # Opening file in text mode (t) returns string type on reading,
        # opening file in binary mode (b) returns bytes type on reading.
        result = None
        with fs.open(url, "rb") as file:
            result = file.read().decode()
        return result
    
    @staticmethod
    def __request_http(url):
        return Loader.__HTTP.request(url)
    
    @staticmethod
    def __request_ftp(url):
        result = None
        match = re.match(r'ftp:[/\\]{2}([^/\\]+)[/\\](.+)', url, re.I)
        if match is not None:
            server = match.group(1)
            resource = match.group(2).replace(r'\\', '/')
            with ftplib.FTP(server) as ftp:
                ftp.login('anonymous', 'anonymous@python.org')
                with tempfile.TemporaryFile('w+t') as file:
                    response = ftp.retrlines('RETR ' + resource, file.write)
                    if response.startswith('226'):
                        file.seek(0)
                        result = file.read()
                ftp.quit()
        return result
    