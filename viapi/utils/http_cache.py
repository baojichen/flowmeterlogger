class http_cache:
    """http cache layer"""
    __CACHE = {}
    
    # Puts the data of the specified URL into the cache.
    # Any previous data is replaced.
    @staticmethod
    def update(url, data):
        http_cache.__CACHE[url] = data

    # Retrieves the cached data for the specified URL.
    # If URL is not cached an exception is thrown.
    @staticmethod
    def get(url):
        if not url in http_cache.__CACHE:
            raise RuntimeError("Resource not cached: {}".format(url))
        return http_cache.__CACHE[url]
    
    # Check if the specified URL is in cache:
    # Returns True if URL is cached, False otherwise.
    @staticmethod
    def has(url):
        result = False
        if url in http_cache.__CACHE:
            result = True
        return result
    
    # Clears the entire cache discarding all data
    @staticmethod
    def invalidate():
        http_cache.__CACHE = {}
