import json

# Creates a string representation of the given (possibly nested) object.
def traverse(obj):
    # Try to parse any string as JSON dictionary
    if isinstance(obj, str):
        try:
            obj = json.loads(obj)
        except:
            pass
    # Handle standard general purpose containers: dict, list, tuple, set
    if isinstance(obj, dict):
        return {key: traverse(value) for key, value in obj.items()}
    if isinstance(obj, list):
        return [traverse(value) for value in obj]
    if isinstance(obj, tuple):
        return tuple([traverse(value) for value in obj])
    if isinstance(obj, set):
        return {traverse(value) for value in obj}
    # At that point we assume it's a scalar type only.
    # Any non-primitive types are converted to string representation first
    # and then convert back into a JSON dictionary by recursion.
    # All other types are returned as-is.
    if not isinstance(obj, (str, int, float, bool)):
        return traverse(str(obj))
    return obj

# Create a dictionary containing the specified key/value pairs
# and return the string representation of that object.
def stringify(*args):
    result = {}
    for name, data in zip(args[0::2], args[1::2]):
        # Stringify data.
        # If data is a container recursively process it.
        result[str(name)] = traverse(data)
    return json.dumps(result, indent = 4)
