import math

# Evaluates the given expression in Python syntax
# using a minimal (safe) function subset.
# If symbols is given as arguments it must be a dict
# mapping local symbols to values, e.g. {"X": 42}
def safe_eval(expr, symbols = None):
    def safe_str(value):
        return str(value)
    def safe_int(value):
        return int(value, 0)
    safe_language = {
        # safe general purpose
        "str": safe_str,
        "int": safe_int,
        "bin": bin,
        "hex": hex,
        "oct": oct,
        "min": min,
        "max": max,
        "round": round,
        # safe math
        "e": math.e,
        "pi": math.pi,
        "inf": math.inf,
        "nan": math.nan,
        "ceil": math.ceil,
        "floor": math.floor,
        "abs": math.fabs,
        "frac": math.trunc,
        "mod": math.fmod,
        "pow": math.pow,
        "exp": math.exp,
        "sqrt": math.sqrt,
        "log": math.log,
        "cos": math.cos,
        "cosh": math.cosh,
        "acos": math.acos,
        "acosh": math.acosh,
        "sin": math.sin,
        "sinh": math.sinh,
        "asin": math.asin,
        "asinh": math.asinh,
        "tan": math.tan,
        "tanh": math.tanh,
        "atan": math.atan,
        "atanh": math.atanh,
        "atan2": math.atan2
    }
    if symbols is not None:
        if not isinstance(symbols, dict):
            raise TypeError('Symbols must be a dict type')
        safe_language.update(symbols)
    try:
        result = eval(expr, {"__builtins__": None}, safe_language)
    except:
        raise RuntimeError('Unable to evaluate expression "{}" (missing symbol or syntax error?)'.format(expr))
    return result
