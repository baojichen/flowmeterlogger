import os
import zipfile

from .fsobj import *

class fs:
    """file system abstraction"""
    
    @staticmethod
    def open(path, mode = "r"):
        result = None
        if path is not None:
            result = fs.__open_internal(fs.__split_path(path), mode)
        # Raise an exception if unable to open specified file
        if result is None:
            raise OSError("No such file or directory: '{}'".format(str(path)))
        return result
    
    @staticmethod
    def __open_internal(path, mode):
        result = None
        if len(path) > 0:
            handle = []
            buffer = path
            i = 0
            while i < len(buffer):
                i = i + 1
                if len(handle) > 0:
                    # Path including at least a single zip archive at this point.
                    # Must use last zip file handle here to dive into path.
                    parent = handle[-1]
                    reldir = fs.__is_member(parent, buffer[:i])
                    if not reldir:
                        fs.__close_internal(handle)
                        return None
                    # This file exists, so we can open it
                    # Never handle last item as zip file.
                    # Even if it is one such a path mean user want to
                    # open the zip file itself as a binary file, not its contents.
                    if len(buffer)-i > 0:
                        # If seems "rb" is not supported for zipfile, must be r,w,x or a
                        h = parent.open(fs.__join_path(reldir), "r")
                        # If the archive member is again an archive we
                        # have to dive deeper into path using its handle
                        if fs.__is_archive(h):
                            buffer = buffer[i:]
                            # If seems "rb" is not supported for zipfile, must be r,w,x or a
                            handle.append(zipfile.ZipFile(h, "r"))
                            i = 0
                        else:
                            h.close()
                else:
                    # Path not including a zip file yet.
                    # We can use normal OS path and file functions here.
                    reldir = fs.__join_path(buffer[:i])
                    if not os.path.exists(reldir):
                        fs.__close_internal(handle)
                        return None
                    if len(buffer)-i > 0:
                        if fs.__is_archive(reldir):
                            buffer = buffer[i:]
                            # If seems "rb" is not supported for zipfile, must be r,w,x or a
                            handle.append(zipfile.ZipFile(reldir, "r"))
                            i = 0
            result = fsobj(fs.__join_path(buffer), handle, mode)
        return result
    
    @staticmethod
    def __close_internal(handle):
        # Close ZipFile handles here
        while len(handle) > 0:
            handle.pop().close()
    
    # Split the given path (as a string) into a list of string fragments.
    @staticmethod
    def __split_path(path):
        waypoints = []
        if isinstance(path, str) and (len(path) > 0):
            # Expand path items like . and .. first
            if path.endswith("/") or path.endswith("\\"):
                trailing = path[-1]
            else:
                trailing = ""
            # It seems that trailing backslash is removed by this function
            path = os.path.abspath(path)
            name = None
            while True:
                # It seems that os.path.split() returns name as ""
                # in case last part is a separator
                path, name = os.path.split(path)
                if (name is None) or (name == ''):
                    waypoints.insert(0, path)
                    break
                waypoints.insert(0, name)
            waypoints[-1] = waypoints[-1] + trailing
        return waypoints
    
    # Join the given list of string fragments into a single path string.
    @staticmethod
    def __join_path(path):
        if isinstance(path, list):
            result = ""
            for waypoint in path:
                result = os.path.join(result, waypoint)
        else:
            result = str(path)
        return result
    
    # Returns True is specified file is a valid archive that can be processed by zipfile package.
    # If file does not exist function returns False.
    @staticmethod
    def __is_archive(path):
        result = False
        if isinstance(path, zipfile.ZipExtFile):
            result = zipfile.is_zipfile(path)
        else:
            if not isinstance(path, str):
                path = str(path)
            if len(path) > 0:
                path = os.path.abspath(path)
                if os.path.exists(path) and os.access(path, os.R_OK) and os.path.isfile(path):
                    result = zipfile.is_zipfile(path)
        return result
    
    # Returns whether the specified item is a file or directory in the archive or base directory.
    # The function returns the name of the item found or None if no such item has been found.
    @staticmethod
    def __is_member(handle, member):
        result = None
        if handle is not None:
            if isinstance(member, list):
                path = fs.__join_path(member)
            else:
                path = str(member)
            # It seems that getinfo() function only can handle unix slashes
            path = path.replace("\\", "/")
            # For a member "abc" we test both file "abc" and directory "abc/"
            table = [path]
            if not path.endswith("/"):
                table.append(path + "/")
            # If not existing this raises KeyError exception
            for obj in table:
                try:
                    if isinstance(handle.getinfo(obj), zipfile.ZipInfo):
                        result = obj
                        break
                except:
                    pass
        return result
