import sys
import os

class os_utils:
    """OS utils"""
    
    # Returns the site-packages directory used.
    # If a library name is given the function returns the
    # site-packages directory containing the specified library.
    # If executed from within a frozen application the function returns None.
    @staticmethod
    def get_site_package_dir(library = None):
        # If we reside within frozen application we have no site-packages
        if getattr(sys, 'frozen', False):
            return None
        # There is a "user" site-packages folder and a "system" site-packages folder.
        # e.g. system folder may reside in
        #    \%PROGRAMFILES%\Python\lib\site-packages
        # and user folder may reside in
        #    \Users\%USERNAME%\AppData\Roaming\Python\Python\site-packages
        # We first search system folder and if not found within that
        # we continue search in user folder.
        dirs = []
        if sys.version_info.major >= 3:
            try:
                import sysconfig
                dirs.append(sysconfig.get_paths()["purelib"])
            except:
                pass
        try:
            import site
            for directory in site.getsitepackages():
                dirs.append(directory)
            for directory in site.getusersitepackages():
                dirs.append(directory)
        except:
            pass
        # Simply guess folder by inspecting executable location
        dirs.append(os.path.join(os.path.split(sys.executable)[0], "/lib/site-packages"))
        # Check directories
        result = None
        for folder in dirs:
            if (folder.find("site-packages") > 0) and os.path.isdir(folder) and os.access(folder, os.W_OK):
                if library is not None:
                    # Check if folder contains requested library
                    lib_dir = os.path.join(folder, library)
                    if os.path.isdir(lib_dir):
                        result = lib_dir
                        break
                else:
                    result = folder
                    break
        return result
