import re
import io
import sys

from .utils import *

class Preprocessor:
    # self.__tokens: dict [str -> str]
    
    # Replace a single token only (False) or parse @TOKEN@ as an
    # expression in Python syntax with minimal (safe) language subset.
    USE_EXT_EVAL = True
    
    # Creates a new preprocessor object.
    # The preprocessor substitutes tokens in the format @TOKEN@ in a (possibly nested) dictionary.
    # The tokens to substitute can be optionally passed as argument:
    # The tokens argument is a dictionary where each key is the case-sensitive
    # name of the token to be replaced by the associated value.
    def __init__(self, tokens = None):
        self.discard()
        self.add(tokens)
    
    # Discards all current tokens
    def discard(self):
        self.__tokens = {}
    
    # Returns whether the specified key(s) exists.
    # A single key can be given as a string type.
    # If multiple keys are given (as a list type) the
    # function returns True if (and only if) all of them exists.
    def exists(self, keys):
        result = False
        if isinstance(keys, str):
            if keys in self.__tokens.keys():
                result = True
        else:
            # If multiple keys are given return True if (and only if) all of them exist.
            # This is done by iterating the list and recurse for its elements.
            # Therefore nested lists are supported.
            if isinstance(keys, (list, tuple, set)):
                if len(keys) > 0:
                    result = True
                    for name in keys:
                        result = result and self.exists(name)
                        if result is False:
                            break
        return result
    
    # Return the value(s) of the specified token(s) as a list.
    # If the specified argument is a string the value is returned as a scalar.
    # Otherwise if the argument is a list type the values are returned as a list.
    # If a requested token does not exist None is returned.
    def get(self, keys):
        result = None
        if isinstance(keys, str):
            if keys in self.__tokens.keys():
                result = self.__tokens[keys]
        else:
            # If multiple keys are given return a list of values.
            # Use recursion to support nested list types.
            if isinstance(keys, (list, tuple, set)):
                result = []
                if len(keys) > 0:
                    for name in keys:
                        result.append(self.get(name))
        return result
    
    # Return the keys of all tokens available as a list.
    # If no tokens are available an empty list is returned.
    def keys(self):
        return list(self.__tokens.keys())
    
    # Add the given tokens (as key/value pairs) possibly replacing old tokens with the same key.
    # The tokens argument is a dictionary where keys are the
    # tokens to replace against the associated values.
    # Tokens are case-sensitive.
    def add(self, tokens):
        if tokens is not None:
            if not isinstance(tokens, dict):
                raise TypeError('Tokens must be a dictionary')
            # Log if replacing tokens
            for key in self.__tokens.keys() & tokens.keys():
                if self.__tokens[key] != tokens[key]:
                    Logger.write('Replacing token "{}": {} -> {}'.format(key, self.__tokens[key], tokens[key]))
            self.__tokens.update(tokens)
    
    # Process the specified dictionary replacing tokens.
    # In the data any string type defined as "@TOKEN@" is substituted against the token value.
    # This can also change the type of the data (e.g. string to integer).
    # If a token does not exist this function raises an exception.
    def process(self, data):
        if isinstance(data, str):
            return self.__replace_string(data)
        # A valid converted JSON file must map to a dict in outermost level
        if not isinstance(data, (dict, list, tuple, set)):
            raise TypeError('Data argument must be a container type or a string')
        return self.__recurse(data)
    
    # Opens the specified source file replacing all tokens and returns the result.
    # A target file can be specified to which modified data is written.
    # The source file is never modified.
    def process_file(self, source, target = None):
        # Read source file data
        if isinstance(source, str):
            with io.open(file=source, mode='r', encoding=sys.getdefaultencoding(), newline='') as handle:
                data = handle.read()
        else:
            if not isinstance(source, io.IOBase):
                raise TypeError('Source file must be string or a file handle supporting read access')
            # We've received an opened file handle.
            # This file however can be opened in binary or text mode.
            # If opened in binary mode read() don't return strings
            # and we need to convert that later.
            with source as handle:
                handle.seek(0)
                data = handle.read()
        # Ensure we've string data and replace tokens
        if isinstance(data, bytes):
            data = self.__replace_string(data.decode(sys.getdefaultencoding()))
        else:
            data = self.__replace_string(str(data))
        # Write-back modified contents to output file.
        if target is not None:
            if isinstance(target, str):
                with io.open(file=target, mode='w', encoding=sys.getdefaultencoding(), newline='') as handle:
                    handle.write(data)
            else:
                # If argument is a file handle write to it
                if not isinstance(target, io.IOBase):
                    raise TypeError('Target file must be a string or a file handle supporting write access')
                with target as handle:
                    handle.seek(0)
                    if isinstance(handle, (io.RawIOBase, io.BufferedIOBase)):
                        handle.write(data.encode(sys.getdefaultencoding()))
                    else:
                        handle.write(str(data))
        return data
    
    # Recursively processing argument replacing tokens.
    def __recurse(self, data):
        # If a dict we iterate over keys otherwise
        # if list or tuple we iterate over values.
        if isinstance(data, dict):
            for key in data.keys():
                data[key] = self.__recurse(data[key])
        else:
            if isinstance(data, (list, tuple)):
                # list, tuple
                for index in range(0, len(data)):
                    data[index] = self.__recurse(data[index])
            else:
                if isinstance(data, set):
                    # Process a set as a list and transform
                    # the result of the substitution back into a set.
                    data = set(self.__recurse(list(data)))
                else:
                    # Any non-container type can be subject to substitution
                    # We do not want type conversion back to str,
                    # so we can't use self.__replace_all() here
                    data = self.__replace_single(data)
        return data
    
    # Replace all tokens within the given string and return the result as a string.
    # If argument is not a string it is returned unmodified.
    def __replace_string(self, value):
        result = value
        if isinstance(value, str):
            # Can't match for ^@ because this reduces first match.
            pattern = r'(@[^@\r\n]+@)(.*)'
            # Iteratively cut away leading match in string, replace it
            # and continue matching the remaining string until
            # there are no more matches.
            # This algorithm prevents multiple substitutions that
            # can happen if we pass the entire input in each iteration.
            result = ""
            rem = value
            match = re.search(pattern, rem, re.MULTILINE+re.DOTALL)
            while match is not None:
                # Return type of self.__replace_single() can be non-string,
                # so we insist on a string representation here
                result += rem[0:match.span()[0]] + str(self.__replace_single(match.group(1)))
                rem = match.group(2)
                match = re.search(pattern, rem, re.MULTILINE+re.DOTALL)
            result += rem
        return result
    
    # Substitute token on given input value.
    # If input value is a string and refers to a token the substituted value is returned.
    # Otherwise the function returns the input value unchanged.
    def __replace_single(self, value):
        result = value
        # String types are candidates for substitution.
        # We have to check type first and then check the string being a valid token ("@TOKEN@").
        if isinstance(value, str):
            token = value.strip()
            match = re.search(r'\A@([^@\r\n]+)@\Z', token)
            if match is not None:
                token = match.group(1)
                if not Preprocessor.USE_EXT_EVAL:
                    # Replace as single token only
                    if not token in self.__tokens.keys():
                        raise RuntimeError('Token "{}" referenced but never defined'.format(token))
                    result = self.__tokens[token]
                else:
                    # Evaluate token as an expression in Python syntax
                    result = safe_eval(token, self.__tokens)
        return result
    
    # Support index syntax: obj['name']
    # Returns the value of the specified token or
    # returns None if no such token exists.
    def __getitem__(self, name):
        return self.get(name)
    
    # Support index syntax: obj['name'] = value
    def __setitem__(self, name, data):
        self.add({name: data})
    
    def __len__(self):
        return len(self.__tokens)
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'tokens', self.__tokens
        )
    
    def __repr__(self):
        return self.__str__()
