import sys

from ..utils import *

class Personality:
    """personality"""
    # self.__users: dict [str(user) -> int(password)]
    # self.__active: str
    # self.__observers: set [Instrument]
    
    # Known user in direction of increasing privilege
    USERS = ['anonymous', 'partner', 'manufacturer', 'root']
    
    # Create a new Personality object.
    # Users is a dictionary mapping user names to passwords.
    def __init__(self, users = None):
        self.__users = {Personality.USERS[0]: 0}
        self.__active = Personality.USERS[0]
        self.__observers = set()
        # create initial users list
        if users is not None:
            self.authenticate(users)
    
    # Returns the current active user (as a string).
    @property
    def user(self):
        return self.__active
    
    # Returns the password of the current active user (as an int).
    @property
    def password(self):
        if self.__active not in self.__users:
            raise RuntimeError('User {} unknown or not available'.format(self.__active))
        return self.__users[self.__active]
    
    # Registers the given observer to be notified when current active user changes within personality.
    def register(self, observer):
        if isinstance(observer, list):
            # register multiple observers
            for obj in observer:
                self.__register(obj)
        else:
            # register a single observer only
            self.__register(observer)
    
    # Removes the specified observer
    def unregister(self, observer):
        if isinstance(observer, list):
            for obj in observer:
                self.__unregister(obj)
        else:
            self.__unregister(observer)
    
    # Authenticate one or multiple users.
    # This is storing user credentials (username and password).
    # If no password is given the function asks for it.
    # The currently active user does not change.
    # However if the password of current active user changes
    # this is propagated to all connected instruments.
    def authenticate(self, user, password = None):
        if isinstance(user, list):
            for who in user:
                self.__authenticate(who, password)
        else:
            if isinstance(user, dict):
                if password is not None:
                    raise ValueError('No password expected if argument is dictionary')
                for who in user.keys():
                    self.__authenticate(who, user[who])
            else:
                self.__authenticate(user, password)
    
    # Remove the specified users from personality.
    # If user is None all users are removed from personality (except for anonymous).
    # It is not possible to remove anonymous user.
    # Returns True if current active user changes by that operation and False otherwise.
    def discard(self, users):
        result = False
        if isinstance(users, list):
            for name in users:
                result |= self.__discard(name)
        else:
            result = self.__discard(users)
        return result
    
    # Change current active user to have specified user privilege.
    # The user argument can be a list containing multiple users.
    # In that case this function picks the best match as the current active user.
    # Any unknown user is assumed to have no privilege at all.
    # The function returns True if a suitable user was available, False otherwise.
    def request(self, user, exact = False):
        if not isinstance(user, list):
            user = [user]
        result = False
        # If current active user is in given list we have nothing to do.
        # Otherwise we try to pick a suitable user.
        if self.__active not in user:
            for name in user:
                # Check if this user is in the list of currently authenticated users
                who = self.__getuser(str(name), exact)
                if who is not None:
                    self.__change(who)
                    result = True
                    break
        else:
            # If current active user is in list specified we're done
            result = True
        return result
    
    # Register the specified observer.
    def __register(self, observer):
        if observer is None:
            raise RuntimeError('Bad observer')
        # This is a set: Adding an instrument twice should have no effect.
        self.__observers.add(observer)
    
    # Removes the specified observer
    def __unregister(self, observer):
        if observer in self.__observers:
            self.__observers.remove(observer)
    
    # Authenticate the specified user.
    # This is storing user credentials (username and password).
    # If no password is given the function asks for it.
    # The currently active user does not change.
    # However if the password of current active user changes
    # this is propagated to all connected instruments.
    def __authenticate(self, user, password = None):
        if not isinstance(user, str):
            raise TypeError('User must be a string type')
        if password is not None:
            if not isinstance(password, int):
                raise TypeError('Password must be an integer type')
            if (password < 0) or (password > 0xFFFFFFFF):
                raise ValueError('Password must be in range: 0 <= password <= 0xFFFFFFFF')
        else:
            # ask for password if None was given
            password = self.__prompt(user)
        # Typically if password changes we have nothing to do.
        # However we may have connected more instruments meanwhile.
        # So in that case we need to propagate user to these new devices.
        self.__users[user] = password
        # If changing current active user we need to propagate new password
        if self.__active == user:
            self.__notify()
    
    # Remove the specified user from personality.
    # If user is None all users are removed from personality (except for anonymous).
    # It is not possible to remove anonymous user.
    # Returns True if current active user changes by that operation and False otherwise.
    def __discard(self, user = None):
        result = False
        if user is not None:
            if not isinstance(user, str):
                raise TypeError('User must be a string type')
            # Silently skip withdrawing anonymous.
            if (user != Personality.USERS[0]) and user in self.__users:
                del self.__users[user]
                if self.__active == user:
                    result = True
        else:
            # If we're anonymous then nothing changes.
            # Otherwise the current active user will change.
            if self.__active != Personality.USERS[0]:
                result = True
            self.__users = {Personality.USERS[0]: 0}
        # If our current active user is removed we fall back to anonymous.
        if result is not False:
            self.__active = Personality.USERS[0]
            self.__notify()
        return result
    
    # Changes the current active user.
    # If the user has already been authenticated by username and password
    # these credentials are written to associated instruments.
    # You may pass an optional password to simultaneously authenticate and login.
    # If that optional password is omitted this function prompts for password.
    def __change(self, user, password = None):
        if not isinstance(user, str):
            raise TypeError('User must be a string type')
        if password is not None:
            if not isinstance(password, int):
                raise TypeError('Password must be an integer type')
            if (password < 0) or (password > 0xFFFFFFFF):
                raise ValueError('Password must be in range: 0 <= password <= 0xFFFFFFFF')
        # If user has not authenticated yet do that (and ask for password if not given)
        if not user in self.__users:
            if password is None:
                password = self.__prompt()
            self.authenticate(user, password)
        self.__active = user
        # Propagate user change to all observers
        self.__notify()
    
    # Notify all registered observers upon user and/or password change.
    # This uses current active user.
    def __notify(self):
        user = self.__active
        if not user in self.__users:
            raise RuntimeError('Unknown user "{}" on calling observers'.format(user))
        password = self.__users[user]
        for observer in self.__observers:
            # First we try to call a method login() on any observer.
            # If that does not work we check if object is callable and then call it.
            # In both cases we pass ourself, the user name (as a string) and password (as an int).
            # We pass Personality object to enable observers to query some more details on the user privilege.
            try:
                observer.login(user, password, self)
            except:
                if callable(observer):
                    try:
                        observer(user, password, self)
                    except:
                        # An exception whould not cancel our notification procedure
                        pass
                else:
                    raise RuntimeError('Invalid observer: Must be callable to receive notification')
    
    # Prompt user to input password
    def __prompt(self, user = None):
        result = None
        if user is not None:
            ask = 'Please enter password for user {}: '.format(user)
        else:
            ask = 'Please enter password: '
        # If running on real terminal prompt user for password,
        # otherwise try to use Tk and if that fails simply raise exception.
        if sys.stdin.isatty():
            # Keep looping until user has input a valid integer
            while result is None:
                value = input(ask)
                try:
                    value = int(value, 0)
                    if (value >= 0) and (value <= 0xFFFFFFFF):
                        result = value
                except:
                    pass
        else:
            try:
                import tkinter.simpledialog
                # If user chooses to cancel this result is None
                result = tkinter.simpledialog.askinteger('Login', ask, minvalue=0, maxvalue=0xFFFFFFFF)
            except:
                pass
            # Give up: Not having a terminal and not having a Tk GUI either
            # or user does not want to enter password
            if result is None:
                raise RuntimeError('Please specify password for user to login')
        return result
    
    # Returns an authenticated user incorporating sufficient privilege
    # to have the same or more access rights than the specified user.
    # If no such user is authenticated the function returns None.
    # If exact argument is set to False the function may return another user
    # having higher privilege than the specified user.
    # Otherwise if exact argument is set to True and
    # given user is not found the function returns None.
    def __getuser(self, user, exact = False):
        result = None
        # First we check if we know such a user
        try:
            which = Personality.USERS.index(user)
        except:
            # This is an unknown user.
            # We assume such users are equivalent to "anonymous" user.
            which = 0
        # If that specific user is available we've nothing more to do
        if Personality.USERS[which] in self.__users:
            result = Personality.USERS[which]
        else:
            # Otherwise we check if personality contains a user having at least the same privilege.
            # Since USERS array is sorted in direction of increasing privilege
            # we keep incrementing the index and check if such a user is part of personality.
            if not bool(exact):
                for index in range(which, len(Personality.USERS)):
                    name = Personality.USERS[index]
                    if name in self.__users:
                        result = name
                        break
        return result
    
    # Create a readable string representation
    def __str__(self):
        # Can't stringify self.__observers which contains Instrument objects.
        # This would cause endless loop.
        return stringify(
            'users', self.__users,
            'active', self.__active
        )
    
    def __repr__(self):
        return self.__str__()
