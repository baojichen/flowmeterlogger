from ..utils import *

from .personality import Personality

class Access:
    """user access rights"""
    # self.__access: dict [str(user) -> list(access_right)]
    
    # Create a new access right object.
    # If no argument is given anyone can read and write.
    def __init__(self, access = None):
        # Access dictionary example:
        # {
        #     "*": ["read"],
        #     "manufacturer": ["read", "write"]
        # }
        if access is None:
            value = {'*': ['read', 'write']}
        else:
            value = access
        # Keys correspond to users.
        # The values associated with them mark the access rights.
        # Check that all values are sane.
        for name in value.keys():
            for right in value.get(name):
                if right not in ['read', 'write']:
                    raise ValueError('Invalid access right')
        self.__access = value
    
    # Check whether specified user can read this register
    def readable(self, user):
        if user in self.__access:
            value = self.__access.get(user)
        else:
            # The requested user is not specified explicitely.
            # So we fall-back to any user.
            if '*' not in self.__access:
                return False
            value = self.__access.get('*')
        if 'read' not in value:
            return False
        return True
    
    # Check whether specified user can write this register
    def writable(self, user):
        if user in self.__access:
            value = self.__access.get(user)
        else:
            # The requested user is not specified explicitely.
            # So we fall-back to any user.
            if '*' not in self.__access:
                return False
            value = self.__access.get('*')
        if 'write' not in value:
            return False
        return True
    
    # Return a list of users permitted access with the given access right.
    # The list is sorted in direction of increasing user privilege.
    # If no user has permission an empty list is returned.
    def users(self, access):
        if not isinstance(access, str):
            raise TypeError('Invalid access right: expecting a string type')
        result = []
        # Note that Personality.USERS is sorted in direction of increasing access rights starting with anonymous.
        for user in Personality.USERS:
            if user != Personality.USERS[-1]:
                # Not root user: Must check that first ...
                value = []
                if user in self.__access:
                    value = self.__access[user]
                else:
                    if '*' in self.__access:
                        value = self.__access['*']
                if access in value:
                    result.append(user)
            else:
                # Root user can do everything
                result.append(user)
        return result
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'access', self.__access
        )
    
    def __repr__(self):
        return self.__str__()
