import os
import sys
import re
import urllib.parse
import validators

from .utils import *

class Location:
    # self.__base: str
    # self.__where: str
    # self.__stack: list (-> str)
    
    # Create a new location object using the given URL.
    # Further resource access is relative to that URL.
    # If path is given (optional) it must be
    # relative to the specified URL.
    def __init__(self, url, *path):
        self.reset(url, *path)

    # Sets a new location discarding any previous state.
    # The given URL is not None it's used as a new location.
    # Further resource access is relative to that URL.
    # If path is given (optional) it must be
    # relative to the specified URL.
    def reset(self, url, *path):
        if url is not None:
            if not isinstance(url, str):
                raise TypeError('URL: expecting a string type')
        else:
            url = self.__basedir()
        where = 'remote'
        base = self.__remote(url)
        if base is None:
            where = 'local'
            base = self.__local(url)
        if base is None:
            raise ValueError('URL does not point to an existing location or is invalid')
        self.__base = base
        self.__where = where
        self.__stack = []
        # Subtile: Need self.__base and self.__where set before calling this
        self.chdir(*path)
    
    # Returns the specified resource (by contents) that resides within the current location.
    # An exception is thrown if the resource does not exist or can't be fetched from remote.
    # If multiple resources are requested a list is returned containing their contents.
    # The contents is returned unboxed if requesting a single resource only.
    def get(self, *resource):
        result = None
        count = len(resource)
        if count > 0:
            if count == 1:
                # If requesting a single resource only
                # we do not box this into a list.
                result = self.__loader(resource[0])
            else:
                result = []
                for obj in resource:
                    result.append(self.__loader(obj))
        return result
    
    # Changes the location using the given relative path.
    # If multiple arguments are given for path they are joined together.
    # The function returns the new location.
    def chdir(self, *path):
        result = self.__base
        if len(path) > 0:
            for directory in path:
                if directory is not None:
                    if not isinstance(directory, str):
                        raise TypeError('Path: expecting a string type')
                    result = self.__join(result, directory)
            # Subtile: Change AFTER new path is fully valid
            self.__base = result
        return result
    
    # Changes the location using the given relative path
    # and pushes/saves the old location to an internal stack.
    # If multiple arguments are given for path they are joined together.
    # The function returns the new location.
    def push(self, *path):
        self.__stack.append(self.__base)
        return self.chdir(*path)
    
    # Restores the location to the last location saved on the internal stack.
    def pop(self):
        if len(self.__stack) > 0:
            self.__base = self.__stack.pop(-1)
        else:
            raise RuntimeError('Location list is empty')
    
    # Get the remote path of a resource.
    # If the resource is local or the URL is invalid None is returned.
    def __remote(self, url):
        if (url is None) or not isinstance(url, str):
            raise TypeError('URL: expecting a string type')
        result = None
        try:
            if validators.url(url):
                # It seems that validators.url() only recognizes http/https/ftp schema.
                # However to be safe we check for file schema explicitely:
                # If pointing to local resource we can't use HTTP to access it.
                # We only return the URL if it's a true remote resource.
                if re.match(r'file:', url, re.I) is None:
                    result = url
        except:
            pass
        return result
    
    # Get the fully qualified local path of a resource.
    # The local path must exist.
    # If the resource is remote or the URL is invalid None is returned.
    def __local(self, url):
        if (url is None) or not isinstance(url, str):
            raise TypeError('URL: expecting a string type')
        result = None
        # Check if the given URL is a local or remote resource
        resource = None
        try:
            # Catch local resources that are specified in URL style
            match = re.match(r'file:[/\\]{2}(localhost)?[/\\](.+)', url, re.I)
            if match is not None:
                if not os.name.lower().startswith('nt'):
                    resource = os.path.realpath('/' + match.group(2))
                else:
                    resource = os.path.realpath(match.group(2))
            else:
                # If seems that validators.url() only recognizes http/https/ftp schema.
                # Returns True if URL is valid but may throw an exception or
                # return a ValidationFailure object if not an URL.
                if not validators.url(url):
                    raise ValueError('Not a valid URL')
        except:
            # Anything that is not a URL can be a local resource.
            # However any broken URL having a valid schema can't.
            # Subtile: Should not match against C:\somewhat
            if re.match(r'[a-z][a-z]+:', url, re.I) is None:
                resource = os.path.realpath(url)
        # Check if the path we've found actually exists
        if resource is not None:
            if os.path.exists(resource):
                result = resource
        return result
    
    # Returns the directory this .py file resides in
    def __basedir(self):
        # For frozen applications __file__ is unavailable.
        # However sys.executable points to the compiled application.
        if not getattr(sys, 'frozen', False):
            # Here we can have normal python invocation or an interactive shell.
            # For interactive shell __file__ can be unavailable too
            # but sys.executable is wrong as well.
            # Fallback to working directory in such cases.
            if '__file__' in locals():
                where = __file__
            else:
                # This returns a directory and should not be processed by os.path.dirname()
                return os.path.realpath(os.getcwd())
        else:
            # We reside in a frozen application
            where = sys.executable
        return os.path.dirname(os.path.realpath(where))
    
    # Returns the specified resource (by contents) that resides within the current location.
    # An exception is thrown if the resource does not exist or can't be fetched from remote.
    def __loader(self, resource):
        if (resource is None) or not isinstance(resource, str):
            raise TypeError('Resource: expecting a string type')
        return Loader.request(self.__join(self.__base, resource))
    
    # Joins the given url with the specified path elements.
    def __join(self, url, *path):
        result = url
        if self.__where == 'remote':
            for directory in path:
                if directory is not None:
                    if not isinstance(directory, str):
                        raise TypeError('Path: expecting a string type')
                    # urllib.parse.urljoin() only joins if last
                    # character of first URL part is a slash.
                    # Otherwise it removes the last path element
                    # before attaching the new path element
                    # effectively replacing the first one.
                    #
                    # Example: 'http://www.test.de/api' join 'v1' gives 'http://www.test.de/v1'
                    if result[-1] != '/':
                        result += '/'
                    # If second URL part starts with a slash the
                    # last element of first URL part is removed.
                    if directory[0] == '/':
                        directory = directory[1:]
                    result = urllib.parse.urljoin(result, directory)
        else:
            # The function os.path.join() does not resolve "." or "..".
            # We need to invoke os.path.realpath() to apply these.
            for directory in path:
                if directory is not None:
                    if not isinstance(directory, str):
                        raise TypeError('Path: expecting a string type')
                    result = os.path.realpath(os.path.join(result, directory))
        return result
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'base', self.__base,
            'where', self.__where,
            'stack', self.__stack
        )
    
    def __repr__(self):
        return self.__str__()
