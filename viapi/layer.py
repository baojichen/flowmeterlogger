from .io import IO

class Layer:
    """register layer handling"""
    # self.__io: IO
    # self.__mask: int
    
    def __init__(self, io):
        if not isinstance(io, IO):
            raise TypeError('IO: expecting IO object')
        self.__io = io
        self.reset()
    
    # Invalidate internal cache values.
    def reset(self):
        self.__mask = None
    
    # Returns the current layer mask.
    # Does not throw and returns zero if unable to read the layer mask.
    # The value is internally cached.
    def get(self):
        if self.__mask is None:
            self.__mask = self.__read_internal(0xFFF0)
        return self.__mask
    
    # Enable the layers specified by given bit-mask.
    # Does not throw and is a no-op internally if layer is None.
    # The write is omitted if the layers are already enabled.
    def enable(self, data):
        if data is not None:
            if self.__mask is None:
                self.get()
            # Skip writing if layer is already enabled
            if data != (self.__mask & data):
                # LAYER_ENABLE
                self.__write_internal(0xFFF1, data)
    
    # Disable the layers specified by given bit-mask.
    # Does not throw and is a no-op internally if layer is None.
    # The write is omitted if the layers are already disabled.
    def disable(self, data):
        if data is not None:
            if self.__mask is None:
                self.get()
            # Skip writing if layer is already disabled
            if data != (~self.__mask & data):
                # LAYER_DISABLE
                self.__write_internal(0xFFF2, data)
    
    # Write register(s):
    # Argument can be scalar or list type.
    def __write_internal(self, addr, data):
        # Box value into list if scalar
        if not isinstance(data, (list, tuple, set)):
            data = [data]
        # Layer registers can be written by anonymous
        # so there's no need to login as a privileged user here
        try:
            # LAYER_DISABLE
            self.__io.write(addr, data)
        except:
            pass
    
    # Read register:
    # Result is returned as scalar value.
    def __read_internal(self, addr):
        result = 0
        try:
            result = self.__io.read(addr, 1)
            if isinstance(result, list):
                result = result[0]
        except:
            pass
        return result
