import os
import sys

def __install(package):
    success = False
    # If we reside in a frozen application we can't install packages this way
    if not getattr(sys, 'frozen', False):
        # If python path include spaces we need to escape using quotes
        if os.system('"' + sys.executable + '" -m pip install '+package) == 0:
            success = True
    if not success:
        print('Warning: Failed to install package '+package)

# Check if all dependent libraries exist.
# If not try to auto-install them.
# Using sys.executable to do this ensures this actually
# uses the Python installation running this script.
try:
    import serial
except:
    # package to use is pyserial, but need to import serial
    __install('pyserial')
    import serial
try:
    import minimalmodbus
except:
    __install('minimalmodbus')
    import minimalmodbus
# Python 2.x: urllib2, Python 3.x: urllib3
if sys.version_info < (3, 0):
    try:
        import urllib2
    except:
        __install('urllib2')
        import urllib2
else:
    try:
        import urllib3
    except:
        __install('urllib3')
        import urllib3
try:
    import validators
except:
    __install('validators')
    import validators
try:
    import packaging
except:
    __install('packaging')
    import packaging

from .utils import *
from .privilege import *
from .register import *
from .db import DB
from .instrument import Instrument
from .io import IO
from .layer import Layer
from .location import Location
from .memory import Memory
from .preprocessor import Preprocessor
from .updater import Updater
