import os
import copy
import json
import packaging.version
import packaging.specifiers

from .utils import *
from .register import *
from .location import Location
from .preprocessor import Preprocessor
from .updater import Updater

class DB:
    """device register database"""
    # self.__repo: dict [result of json.loads()]
    # self.__location: Location
    # self.__data: dict [result of json.loads()]
    # self.__regs: dict (-> Register)
    # self.__version: str
    # self.__device: str
    # self.__preprocessor
    # self.__tree: dict (str: device -> list: version)
    
    # This is the web (download) URL of the register database.
    REPOSITORY = 'http://api.voegtlin.com'
    # Relative location (e.g. directory or network path) in which index.json is located for a given URL.
    SUBDIR = 'db'
    
    # Create an object loading all register definitions for given device and device version.
    # If no device version is given the most recent device version is assumed.
    # If no URL is given register definitions are searched in a local repository.
    # Otherwise register definitions are searched in repository pointed to by given URL.
    def __init__(self, device, version = None, url = None):
        if (url is not None) and not isinstance(url, str):
            raise TypeError('URL: expecting a string type')
        # If URL is not specified we check site-package directory for register definitions.
        # This must be a .zip file ("db.zip") containing a subdirectory DB.SUBDIR with register definitions.
        if url is None:
            url = DB.REPOSITORY
            # Within frozen application this returns None
            site = os_utils.get_site_package_dir(Updater.MODULE)
            if site is not None:
                repo = os.path.join(site, DB.SUBDIR +'.zip')
                if os.path.exists(repo):
                    url = repo
        # Let location point into root of our database and load the repository
        self.__location = Location(url, DB.SUBDIR)
        self.__repo = json.loads(self.__location.get('index.json'))
        # Search and load register definitions of requested device
        self.__regs = {}
        if device is not None:
            self.load(device, version)
    
    # Load all register definitions for a given device and device version from current repository.
    # If no device version is given the function loads registers definitions of the most recent device version.
    # If no device is given (it is None or an empty string) current register definitions are discarded.
    def load(self, device, version = None):
        if device is not None:
            if not isinstance(device, str):
                raise TypeError('Device: expecting a string type')
            if (version is not None) and not isinstance(version, str):
                raise TypeError('Version: expecting a string type')
            # Discard any previously loaded stuff first
            self.discard()
            if len(device) > 0:
                # Load requested device registers and preprocessor tokens
                self.__device = device
                self.__data, self.__version = self.__device_loader(device, version, None)
                # Apply preprocessor substitutions and create registers
                self.__data = self.__preprocessor.process(self.__data)
                self.__regs = self.__make_regs(self.__data)
                # Check if requested device is abstract and therefore can't be instantiated
                if self.__is_abstract(self.__device, self.__version):
                    raise RuntimeError('Unable to instantiate abstract device "{}" (version {})'.format(self.__device, self.__version))
        else:
            self.discard()
    
    # Checks if the specified register exists.
    # If the register exists the function returns True, otherwise False.
    def exists(self, name):
        if not name in self.__regs:
            return False
        return True
    
    # Returns the register identified by given name as a dict.
    # If no such register exists None is returned.
    # If argument is None all registers are returned as a dict.
    def query(self, name = None):
        if name is None:
            return self.__regs
        if not self.exists(name):
            return None
        # Can do a shallow copy here since Register objects use copy too
        return copy.copy(self.__regs.get(name, None))
    
    # Discards all registers currently loaded.
    def discard(self):
        self.__device = None
        self.__version = None
        self.__data = {}
        self.__regs = {}
        self.__tree = {}
        self.__preprocessor = Preprocessor()
    
    # Return a dictionary containing a mapping of all non-abstract devices and their versions.
    # This can be used to manually choose a specific device and device version.
    @property
    def devices(self):
        result = {}
        for device in self.__repo:
            for version in self.__repo[device].keys():
                # Check if device is abstract and skip these
                if not self.__repo[device][version].get('abstract', False):
                    if not device in result:
                        result[device] = []
                    result[device] += [version]
        return result
    
    # Returns the preprocessor object in use
    @property
    def preprocessor(self):
        return self.__preprocessor
    
    @property
    def data(self):
        # Defensive copy: Don't want caller to mutate us.
        # Need a deep copy since that is basically a nested type.
        return copy.deepcopy(self.__data)
    
    @property
    def regs(self):
        # Defensive copy: Don't want caller to mutate us.
        return copy.copy(self.__regs)
    
    @property
    def device(self):
        return self.__device
    
    @property
    def version(self):
        return self.__version
    
    # Resolves the given key returning associated device name and version.
    # If multiple device versions share the same key the most recent version is returned.
    # If a matching device is not found ('generic', None) is returned.
    # Tries a best-fit on the optional specified software version.
    # That is, either a perfect match on the specified software version
    # or the most recent software version not newer than the specified one.
    def resolve(self, key, software = None):
        result = ('generic', None)
        if key is not None:
            if not isinstance(key, int):
                raise TypeError('Key must be an integer type')
            if (software is not None) and not isinstance(software, str):
                raise TypeError('Software version must be a string type (e.g. "1.0.0")')
            # Get a pre-filtered list that only contains devices with matching keys.
            # In this list devices are first sorted in order of appearance in repository
            # and next for device versions with most recent version first.
            devices = self.__get_devices(key)
            if (devices is not None) and (len(devices) > 0):
                # If no particular software version is requested we return the most recent version we have
                if software is None:
                    return (devices[0]['device'], devices[0]['version'])
                # If a particular software version is requested:
                # a) Check for exact software version match first.
                #    An exact match means either the specific software version matches
                #    does a software version is not specified in repository
                #    which will match any version.
                # b) Otherwise return the most recent software version that is
                #    not newer than the software version specified.
                best = None
                ours = packaging.version.parse(software)
                for theirs in devices:
                    # If no software version is specified in repository
                    # this always is considered being an exact match.
                    if theirs['software'] is None:
                        return (theirs['device'], theirs['version'])
                    # e.g. ['1.2.3', '2.3.4', ...]
                    for candidate in [packaging.version.parse(i) for i in list(theirs['software'])]:
                        if candidate == ours:
                            return (theirs['device'], theirs['version'])
                        if candidate < ours:
                            # Was no exact match but maybe it's best fit.
                            # We continue search in this case
                            if (best is None) or (packaging.version.parse(best['software']) < candidate):
                                best = theirs
                if best is not None:
                    result = (best['device'], best['version'])
        return result
    
    # Return a list of device keys.
    # These keys can be used to auto-detect devices.
    # The result value is a list of dictionaries
    # each element being in format:
    #   {
    #       'device': <name>,
    #       'version': <version>,
    #       'key': <key>,
    #       'software': <software>,
    #       'hardware': <hardware>
    #   }
    def __get_devices(self, select = None):
        result = []
        # As of python version 3.7+ the dictionary
        # keeps the insertion order for keys.
        # So the resulting list matches the order
        # of definition in repository.
        for device in self.__repo:
            # Get an ordered list (by product version) for device
            # and join into list containing all devices.
            value = self.__get_device(device, select)
            if value is not None:
                result = result + value
        return result
    
    # Get device identification data of the specified device
    # from repository (sorted by product version, most recent version first).
    # If there is no such device the function returns None.
    def __get_device(self, device, select = None):
        if not device in self.__repo:
            return None
        # a) Fill the list
        result = []
        # e.g. "device" -> "1.0.0" -> { "key", "software", "hardware" }
        #               -> "1.0.1" -> { "key", "software", "hardware" }
        for version in self.__repo[device]:
            # Key to match against register MEM_MIRRORS
            key = None
            if 'key' in self.__repo[device][version]:
                key = int(self.__repo[device][version]['key'], 0)
            # Skip devices not matching given key
            if (select is not None) and (key != select):
                continue
            # Software (firmware) version(s) this device register specification corresponds to.
            # This is a list of versions or None if no software version is specified.
            software = self.__repo[device][version].get('software', None)
            # Hardware version(s) this device register specification corresponds to.
            # This is a list of versions or None if no hardware version is specified.
            hardware = self.__repo[device][version].get('hardware', None)
            result.append({'device': device, 'version': version, 'key': key, 'software': software, 'hardware': hardware})
        # b) Sort the list (by product version), most recent version first
        result.sort(key = lambda obj: packaging.version.parse(obj['version']), reverse = True)
        return result
    
    # Load the register definitions for specified device and requested version.
    # If registers argument is given it specifies the register definition files to load.
    # If this argument is omitted all associated register definition files are loaded.
    # The function returns the joined register definition files
    # (as a joined dictionary) and the actual version of the device.
    # The device version is interesting if no specific version was requested and this function picks one.
    def __device_loader(self, device, version = None, registers = None):
        if isinstance(registers, str):
            registers = [registers]
        Logger.write('Start resolving device "{}" (version {})'.format(device, version))
        result = {}
        # Lookup matching device
        name = device.lower()
        if not name in self.__repo:
            raise RuntimeError('No such device found in repository: {}'.format(device))
        key = self.__best_version(self.__repo[name], version)
        # Protect against recursive inheritance
        if self.__update_tree(device, key):
            return result, key
        # Check if this device requires newer API version
        need_api = self.__repo[name][key].get('api', None)
        if not Updater.supports(need_api):
            raise RuntimeError('Device "{}" (version {}) requires API version {} (please update)'.format(device, key, need_api))
        # At this point we've found a matching version.
        # We first process inheritance and process all base devices.
        if 'inherits' in self.__repo[name][key]:
            all_bases = self.__repo[name][key]['inherits']
            # A dictionary where each key is the base device ("class")
            # from which the current device inherits registers.
            # Can be a dict because device names must be unique
            # and therefore each key is unique then too.
            # Each key is the device name associated is a dict specifying what to inherit.
            for parent in all_bases:
                base = all_bases[parent]
                if isinstance(base, dict):
                    v = base.get('version', None)
                    r = base.get('registers', None)
                else:
                    # Assume str type, so only device name is given (and version is None)
                    v = None
                    r = None
                # Recurse to load registers
                Logger.write('Inherit register(s) from device "{}" (version {})'.format(parent, v))
                jsondata, v = self.__device_loader(parent, v, r)
                result.update(jsondata)
        # Load register definition files associated with this device
        self.__location.push(name, key)
        if 'registers' in self.__repo[name][key]:
            all_regs = self.__repo[name][key]['registers']
            if registers is not None:
                # Caller has requested only a specific set of registers:
                # Check if each of them exists first before loading.
                for r in registers:
                    if not r in all_regs:
                        raise RuntimeError('Register "{}" not found'.format(r))
            else:
                # Caller has NOT requested any specific register so we load all.
                registers = all_regs
            jsondata = self.__json_loader(registers)
        else:
            if registers is not None:
                raise RuntimeError('Registers requested but this device has no registers to load')
            jsondata = {}
        result.update(jsondata)
        self.__location.pop()
        # Update preprocessor tokens
        # By default we always make name and version of the project available.
        self.__preprocessor.add({'NAME': name, 'VERSION': key})
        if 'config' in self.__repo[name][key]:
            tokens = self.__repo[name][key]['config']
        else:
            tokens = None
        self.__preprocessor.add(tokens)
        # Log the number of accumulated registers and tokens (including base devices)
        Logger.write('{} register(s) and {} preprocessor token(s) for device "{}" (version {}) loaded'.format(
           len(result), len(self.__preprocessor), device, key)
        )
        return result, key
    
    # Build list of base devices and check for cyclic inheritance
    def __update_tree(self, device, version):
        result = False
        if not device in self.__tree:
            self.__tree[device] = []
        if not version in self.__tree[device]:
            self.__tree[device].append(version)
        else:
            Logger.write('Possible cyclic inheritance of device "{}" (version {})'.format(device, version))
            result = True
        return result
    
    # Convert the given JSON dictionary to a list of Register objects
    def __make_regs(self, regs):
        # Make and check Register objects here
        result = {}
        if regs is not None:
            for name in regs.keys():
                result[name] = Register(regs[name], name)
            self.__check(result)
        return result
    
    # Load a list of JSON files from current location
    # and returns their contents as a dictionary.
    def __json_loader(self, files):
        if isinstance(files, str):
            files = [files]
        result = {}
        for place in files:
            data = json.loads(self.__location.get(place))
            intersect = result.keys() & data.keys()
            if len(intersect) > 0:
                Logger.write('Replacing registers: {}'.format(intersect))
            result.update(data)
        return result
    
    # Obtains the best version match for a particular device.
    def __best_version(self, device, version = None):
        # Get list of versions for given device sorted in
        # direction of increasing (most recent) versions
        ver = []
        for item in device.keys():
            ver.append(packaging.version.parse(item))
        # First list item stores most recent version
        ver.sort(reverse=True)
        if len(ver) <= 0:
            raise RuntimeError('No register definitions for specified device')
        # Best-match version
        key = None
        if version is None:
            # If no version is given use most recent version we can find.
            key = str(ver[0])
        else:
            try:
                # First we try to interpret version argument as a specifier:
                # This raises InvalidSpecifier exception for strings like "1.0.0"
                our = packaging.specifiers.SpecifierSet(version)
                for item in ver:
                    if item in our:
                        key = str(item)
                        break
            except:
                # Try exact version matching assuming this is a simple version string
                our = packaging.version.parse(version)
                for item in ver:
                    if item == our:
                        key = str(item)
                        break
        # If we fail to find a version matching the users request we raise an exception.
        if key is None:
            raise RuntimeError('Unable to find matching device version: {}'.format(version))
        return key
    
    # Check the specified list of register definitions.
    # For instance this function checks for overlapping register definitions.
    def __check(self, registers):
        lookup = []
        # Check for overlapping address ranges.
        # To do that:
        # a) We create a list with [address, words, name] pairs and sort it (by address).
        # b) We then loop through this list incrementing a location counter.
        # c) If this location counter is at some point above
        #    the following register address these registers overlap.
        for name in registers:
            reg = registers.get(name)
            # Don't add registers marked as alias to the list.
            # These registers will overlap other registers for sure
            # but that is intended and is not an error.
            if not reg.alias:
                address = reg.address
                request = Array(reg.type.size(), reg.count, reg.element)
                # iterate() with no arguments iterates through entire register
                for blob in request.iterate():
                    # blob: [offset (bytes), number of elements, size of elements (in bytes)]
                    offset = blob[0]
                    size = blob[2]
                    # MODBUS address range has word addressing while external memory range use byte addressing.
                    # To be able to handle both we transform byte sizes into words within MODBUS address range.
                    if (address & 0xF0000000) == 0:
                        lookup.append([address + (offset >> 1), (size + (size % 2)) >> 1, name])
                    else:
                        lookup.append([address + offset, size, name])
        # Sort (by address)
        lookup.sort()
        # Look for two neighbor elements x,y that overlap: addr_x + count_x > addr_y
        addr = 0
        last = None
        for item in lookup:
            if addr > item[0]:
                # last can't be None here because initially addr=0 and any register address < 0 is catched long before
                Logger.write('Overlapping address range of {} at {} with {}'.format(item[2], hex(item[0]), last[2]))
            addr = item[0] + item[1]
            last = item
    
    # Returns whether the given device having specified device version is abstract.
    # Abstract devices can't be instantiated and are used for inheritance only.
    # The function returns False if the given device is not found.
    def __is_abstract(self, device, version):
        result = False
        if device in self.__repo:
            if version in self.__repo[device]:
                result = self.__repo[device][version].get('abstract', False)
        return result
    
    # Support index syntax: obj['reg']
    def __getitem__(self, name):
        return self.query(name)
    
    # Support len() function
    def __len__(self):
        return len(self.__regs)
    
    # Support iteration
    def __iter__(self):
        return iter(self.__regs.keys())
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'location', self.__location,
            'repo', self.__repo,
            'device', self.__device,
            'version', self.__version,
            'data', self.__data,
            'regs', self.__regs,
            'tree', self.__tree,
            'preprocessor', self.__preprocessor
        )
    
    def __repr__(self):
        return self.__str__()
