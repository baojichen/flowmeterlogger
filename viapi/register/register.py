import copy

from ..utils import *
from ..privilege import *

from .datatype import DataType
from .cache import Cache

class Register:
    """device register"""
    # self.__address: int
    # self.__type: DataType
    # self.__count: list (int)
    # self.__element: list (int)
    # self.__access: Access
    # self.__alias: bool
    # self.__cache: Cache
    # self.__name: str (or None)
    # self.__delay: dict (str:int)

    __KEYS = ['address', 'type', 'endian', 'count', 'element', 'access', 'alias', 'cache', 'delay']

    # Create a new device register using given dict
    def __init__(self, data, name = None):
        # Carefully check dependencies if changing order
        self.__address = self.__get_address(data)
        self.__type = self.__get_type(data)
        # In register definition the following notation is used for count and element:
        # index:    0   1   2
        # list:    [24, 10, 2]
        #              [10, 2]
        #                  [2]
        #                   ^-- innermost dimension
        # The lists must be reversed so that index 0 always points to innermost dimension.
        self.__count = self.__get_count(data)
        self.__element = self.__get_element(data)
        self.__access = self.__get_access(data)
        self.__alias = self.__get_alias(data)
        self.__delay = self.__get_delay(data)
        # name (optional)
        if name is not None:
            self.__name = str(name)
        else:
            self.__name = '<None>'
        # Cache pragma
        self.__cache = self.__get_cache(self.__name, self.__address, data)
        # Check if we're ignoring fields of the given dictionary.
        # That might indicate a typo in the underlying register definitions.
        diff = set(data.keys()) - set(self.__KEYS)
        if len(diff) > 0:
            for element in diff:
                Logger.write('Ignoring element "{}" in register {} at {}'.format(element, self.__name, hex(self.__address)))
    
    # Checks and returns the register address
    def __get_address(self, data):
        if not 'address' in data:
            raise ValueError("Missing 'address' key")
        address = int(str(data.get('address')), base = 0)
        # Since we artifially use addresses above 0xFFFF to point
        # into external memories there's no longer an 16-bit upper limit
        if (address < 0) or (address > 0xFFFFFFFF):
            raise ValueError("Invalid 'address' value: {}".format(hex(address)))
        return address
    
    # Checks and returns the register type
    def __get_type(self, data):
        # endianness (optional): default to BIG endian
        if 'endian' in data:
            endian = data.get('endian').upper()
            if not endian in [DataType.ENDIAN_BIG, DataType.ENDIAN_LITTLE]:
                raise ValueError("Invalid 'endian' value: Must be 'BIG' or 'LITTLE'")
        else:
            endian = DataType.ENDIAN_BIG
        if not 'type' in data:
            raise ValueError("Missing 'type' key")
        return DataType(data.get("type"), endian)
    
    # Checks and returns the register count (used by array registers)
    def __get_count(self, data):
        # We put count in a list to support multidimensional arrays.
        # Input is specified as [outer-dimension, ..., inner-dimension].
        # We reverse that later so that iterating the list keeps increasing dimension.
        if 'count' in data:
            # Auto-wrap scalar into list
            count = data['count']
            if not isinstance(count, list):
                count = [count]
            # Convert list elements to int
            for index in range(0, len(count)):
                value = int(str(count[index]), base = 0)
                if value <= 0:
                    raise ValueError("Invalid 'count' value: {}".format(value))
                count[index] = value
            # Index 0: outer-dimension -> inner-dimension
            count.reverse()
        else:
            count = [1]
        return count
    
    # Checks and returns the register element sizes (used by array registers)
    def __get_element(self, data):
        # element size (optional): default to size of type if nothing is given
        # We put element in a list to support multidimensional arrays.
        # Input is specified as [outer-dimension, ..., inner-dimension].
        # We reverse that later so that iterating the list keeps increasing dimension.
        size = self.__type.size()
        if 'element' in data:
            # Auto-wrap scalar into list
            element = data['element']
            if not isinstance(element, list):
                element = [element]
            # Convert list elements to int
            for index in range(0, len(element)):
                value = int(str(element[index]), base = 0)
                if value <= 0:
                    raise ValueError("Invalid 'element' value: {}".format(value))
                element[index] = value
            # Index 0: outer-dimension -> inner-dimension
            element.reverse()
        else:
            element = [size]
        # In each dimension element specifies the size in bytes.
        # Check element sizes and ensure that:
        #    0: typesize <= element[0]
        #    1: element[0]*count[0] <= element[1]
        #    2: element[1]*count[1] <= element[2]
        #   ...
        if len(element) > len(self.__count):
            raise ValueError('Dimension of element size (={}) must be equal or smaller than dimension of count (={})'.format(len(element), len(self.__count)))
        value = size
        for index in range(0, len(element)):
            if value > element[index]:
                raise ValueError('Element size (={}) must be greater or equal than type size (={})'.format(element[index], value))
            # Since element must be equal or greater than value
            # we know that
            #    max(value, element[index]) == element[index]
            value = element[index] * self.__count[index]
        return element
    
    # Checks and returns the register access
    def __get_access(self, data):
        # access (optional): if not given assume reading/writing by anyone
        if 'access' in data:
            value = data.get('access')
        else:
            value = None
        return Access(value)
    
    # Checks and returns whether register is an alias
    def __get_alias(self, data):
        # alias (optional): if not given default to False
        if 'alias' in data:
            result = bool(data.get('alias'))
        else:
            result = False
        return result
    
    # Returns cache pragma
    def __get_cache(self, name, address, data):
        if 'cache' in data:
            result = Cache(name, address, data.get('cache'))
        else:
            result = Cache(name, address)
        return result

    # Checks and returns delay time (in ms) for register operations
    def __get_delay(self, data):
        delay = {'read': 0, 'write': 0}
        if 'delay' in data:
            value = data.get('delay')
            if isinstance(value, dict):
                # Get intersection of required and available keys
                for key in delay.keys():
                    if key in value.keys():
                        delay[key] = int(str(value[key]), base = 0)
            else:
                delay = {'read': int(str(value), base = 0), 'write': int(str(value), base = 0)}
            # Check if delay values for read and write are plausible
            for item in delay.values():
                if item < 0:
                    raise ValueError("Negative 'delay' value: {}".format(item))
        return delay

    # Return the name of the register (-> string).
    @property
    def name(self):
        return self.__name
    
    # Returns if that register is an alias of another register (-> bool).
    # This is a hint that the register is allowed to overlap address ranges of other registers.
    @property
    def alias(self):
        return self.__alias
    
    # Readonly access to register address (-> int)
    @property
    def address(self):
        return self.__address
    
    # Readonly access to type (-> DataType)
    @property
    def type(self):
        # No defensive copy required: Currently DataType objects are immutable
        return self.__type
    
    # Readonly access to element size in bytes (-> int)
    @property
    def size(self):
        return self.__type.size()
    
    # Readonly access to number of elements (-> list of int).
    # This is a list (e.g. used to define multi-dimensional arrays)
    # where each element represents one dimension.
    # Index zero in list always refers to the innermost dimension.
    @property
    def count(self):
        # Defensive copy: Don't want caller to mutate us
        return copy.copy(self.__count)
    
    # Readonly access to access rights
    @property
    def access(self):
        # No defensive copy required: Currently Access objects are immutable
        return self.__access

    @property
    def delay(self):
        return self.__delay

    # Readonly access to cache pragma
    @property
    def cache(self):
        # No defensive copy required: Currently Cache objects are immutable
        return self.__cache
    
    # Returns the element size in bytes (-> list of int).
    # This is relevant for array registers only.
    # It can be used to insert gaps/padding between two array elements.
    # Index zero in list always refers to the innermost dimension.
    @property
    def element(self):
        # Defensive copy: Don't want caller to mutate us
        return copy.copy(self.__element)
    
    def __sort_okay(self, other):
        return hasattr(other, 'address') and hasattr(other, 'name')
    
    def __eq__(self, other):
        if not self.__sort_okay(other):
            return NotImplemented
        return (self.address == other.address) and (self.name == other.name)
    
    def __ne__(self, other):
        if not self.__sort_okay(other):
            return NotImplemented
        return not self.__eq__(other)
    
    # Needed to support built-in sort
    def __lt__(self, other):
        if not self.__sort_okay(other):
            return NotImplemented
        if self.address == other.address:
            return self.name < other.name
        return self.address < other.address
    
    def __le__(self, other):
        if not self.__sort_okay(other):
            return NotImplemented
        return self.__lt__(other) or self.__eq__(other)
    
    def __gt__(self, other):
        if not self.__sort_okay(other):
            return NotImplemented
        if self.address == other.address:
            return self.name > other.name
        return self.address > other.address
    
    def __ge__(self, other):
        if not self.__sort_okay(other):
            return NotImplemented
        return self.__gt__(other) or self.__eq__(other)
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'name', self.__name,
            'alias', self.__alias,
            'address', hex(self.__address),
            'type', self.__type,
            'size', self.__type.size(),
            'count', self.__count,
            'access', self.__access,
            'element', self.__element,
            'cache', self.__cache,
            'delay', self.__delay
        )
    
    def __repr__(self):
        return self.__str__()
