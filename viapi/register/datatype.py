import re
import copy
import struct

from ..utils import *

class DataType:
    """data type"""
    # self.__type: tuple (str)
    # self.__size: tuple (int)
    # self.__endian: str
    
    ENDIAN_BIG = 'BIG'
    ENDIAN_LITTLE = 'LITTLE'
    
    # Size (in bytes) of known types
    __repository = { 'CHAR': 1, 'U8': 1, 'U16': 2, 'U32': 4, 'U64': 8, 'S8': 1, 'S16': 2, 'S32': 4, 'S64': 8, 'F32': 4, 'F64': 8, 'STRING': 0, 'UNSIGNED': 0, 'SIGNED': 0 }
    
    # Create a new type object
    def __init__(self, data, endian = 'BIG'):
        if not isinstance(endian, str):
            raise TypeError('Must select BIG or LITTLE endian as a string type')
        endian = endian.upper()
        if endian not in [DataType.ENDIAN_BIG, DataType.ENDIAN_LITTLE]:
            raise ValueError('Must be BIG or LITTLE endian')
        # This always returns a list of tuples (even if a single type is given)
        # Split (type, size) tuples
        self.__type, self.__size = zip(*self.__parse(data, []))
        self.__endian = endian
    
    # Parses the given (possibly nested) type definition
    # returning a single flattend list of types.
    def __parse(self, data, result):
        if isinstance(data, list):
            # Argument is a (possibly nested) list of types.
            # Recursively calling this function "flattens" that list.
            for item in data:
                result = self.__parse(item, result)
        else:
            # If not a list we expect a string representation of the type
            if not isinstance(data, str):
                raise TypeError('Invalid type: expecting a string type')
            size = 0
            typename = data.upper().strip()
            # Get type and extract any size specifier appended
            match = re.match(r'([^<]+)<(\d+)>$', typename, re.I)
            if match is not None:
                size = int(match.group(2), base = 0)
                if size <= 0:
                    raise ValueError('Invalid size specifier {} attached to type'.format(size))
                typename = match.group(1).strip()
            if not typename in DataType.__repository:
                raise ValueError('Invalid type: unknown type name')
            # Check type specifier
            if DataType.__repository[typename] in [None, 0]:
                if size in [None, 0]:
                    # Type needs a size specifier but we don't have one
                    raise ValueError('Type {} needs size specifier'.format(typename))
            else:
                if (size != 0) and (size != DataType.__repository[typename]):
                    # Type doesn't need a size specifier but we have a wrong one given
                    raise ValueError('Type {} does not match size specifier {}'.format(typename, size))
            # Return as tuple (type, size)
            result.append((typename, size))
        return result
    
    # Returns the (accumulated but unaligned) size in bytes.
    # If no index is given the size of the entire type (all elements in list) is returned.
    # Otherwise the given index is expected to be an integer or range
    # to the elements for which the accumulated size is requested.
    def size(self, index = None):
        # If index is None return entire size
        if index is None:
            N = range(0, len(self.type))
        else:
            if isinstance(index, int):
                N = range(index, index+1)
            else:
                if not isinstance(index, range):
                    raise TypeError('Index must be a valid range')
                N = index
        result = 0
        for i in N:
            if i >= len(self.type):
                raise RuntimeError('Invalid index')
            typename = self.type[i]
            if not DataType.__repository[typename] in [None, 0]:
                result = result + DataType.__repository[typename]
            else:
                # This is a type requiring a type specifier
                result = result + self.__size[i]
        return result
    
    # Returns the list of types.
    @property
    def type(self):
        # Defensive copy: Don't want caller to mutate us
        return copy.copy(self.__type)
    
    # Returns the endianness.
    @property
    def endian(self):
        return self.__endian
    
    # Returns the endianness of the type as a string.
    # This is one value of DataType.ENDIAN_BIG or DataType.ENDIAN_LITTLE defaulting to big endian.
    # If the optional query argument is given
    # the function returns a boolean type
    # indicating if the type has the given endianness.
    # The query argument must be either DataType.ENDIAN_BIG or DataType.ENDIAN_LITTLE.
    def endianness(self, query = None):
        if query is None:
            # Return endianness as a string type
            result = DataType.ENDIAN_BIG
            if self.__endian == DataType.ENDIAN_LITTLE:
                result = DataType.ENDIAN_LITTLE
        else:
            if not isinstance(query, str):
                raise TypeError('Must query using a string type')
            result = False
            if self.__endian == query:
                result = True
        return result
    
    # Converts the given list of bytes to the type represented by this object.
    # The conversion result is returned as a list.
    # If the given byte array is empty an empty list is returned.
    def unserialize(self, buffer):
        if not isinstance(buffer, list):
            buffer = [buffer]
        result = []
        if len(buffer) <= 0:
            return result
        typeindex = 0
        offset = 0
        while offset < len(buffer):
            typename = self.type[typeindex]
            size = self.size(typeindex)
            # Must increment typeindex first because we invoke continue in this loop.
            typeindex = (typeindex + 1) % len(self.type)
            # Handle string and character types first: They ignore endianness
            if typename == "STRING":
                value = ''
                for i in range(0, size):
                    value += chr(buffer[offset])
                    offset = offset + 1
                    if offset >= len(buffer):
                        break
                result.append(self.__crop(value))
                continue
            if typename == "CHAR":
                result.append(chr(buffer[offset]))
                offset = offset + 1
                continue
            # Accumulate appropriate size of bytes for current type.
            # Since we respect endianness at that point already everything
            # following this is independent of endianness.
            value = 0
            array = []
            for i in range(0, size):
                if offset < len(buffer):
                    byte = buffer[offset]
                else:
                    byte = 0
                if self.endianness(DataType.ENDIAN_BIG):
                    # Big endian byte order
                    value = (value << 8) | byte
                    array.append(byte)
                else:
                    # Little endian byte order
                    value |= byte << (i * 8)
                    array.insert(0, byte)
                offset = offset + 1
            # Integer and floating point type conversions
            # Always big endianness is okay: we already changed byte order
            if typename == "F32":
                # struct.unpack() always returns a tuple!
                result.append(struct.unpack('>f', bytearray(array))[0])
                continue
            if typename == "F64":
                # struct.unpack() always returns a tuple!
                result.append(struct.unpack('>d', bytearray(array))[0])
                continue
            # At this point we have an integer type that may be signed or unsigned.
            # For unsigned types we can append the value directly.
            # For signed types we need a conversion here.
            if typename.startswith("S"):
                magic = 0x80 << ((size - 1) * 8)
                value = (value ^ magic) - magic
            result.append(value)
        return result
    
    # Converts the given list of values (having types represented by this object) into a list of bytes.
    def serialize(self, buffer):
        if not isinstance(buffer, list):
            buffer = [buffer]
        # Emit warnings here if some values don't fit the type
        self.__check(buffer)
        result = []
        if len(buffer) <= 0:
            return result
        typeindex = 0
        offset = 0
        while offset < len(buffer):
            typename = self.type[typeindex]
            size = self.size(typeindex)
            # Must increment typeindex first because we invoke continue in this loop.
            typeindex = (typeindex + 1) % len(self.type)
            value = buffer[offset]
            offset = offset + 1
            # Handle string and character types first: They ignore endianness
            if typename == "STRING":
                for i in range(0, size):
                    if i < len(value):
                        byte = ord(value[i])
                        # Replace multi-byte characters (we can't handle those) by '?'
                        if byte > 255:
                            byte = ord('?')
                    else:
                        byte = 0
                    result.append(byte)
                continue
            if typename == "CHAR":
                byte = ord(value)
                # Replace multi-byte characters (we can't handle those) by '?'
                if byte > 255:
                    byte = ord('?')
                result.append(byte)
                continue
            # Integer and floating point type conversions
            if self.endianness(DataType.ENDIAN_BIG):
                # Big endian byte order
                if typename == "F32":
                    for value in struct.pack('>f', float(value)):
                        result.append(value)
                    continue
                if typename == "F64":
                    for value in struct.pack('>d', float(value)):
                        result.append(value)
                    continue
                # At this point we have an integer type.
                # We split it into single bytes.
                value = int(value)
                for pos in range(0, size):
                    result.append(0xFF & (value >> (8 * (size - (pos + 1)))))
            else:
                # Little endian byte order
                if typename == "F32":
                    for value in struct.pack('<f', float(value)):
                        result.append(value)
                    continue
                if typename == "F64":
                    for value in struct.pack('<d', float(value)):
                        result.append(value)
                    continue
                # At this point we have an integer type.
                # We split it into single bytes.
                value = int(value)
                for pos in range(0, size):
                    result.append(0xFF & (value >> (8 * pos)))
        return result
    
    # Check if the specified value fits into the underlying type and
    # logs a warning if exceeding the limits of the underlying type.
    def __check(self, data):
        if not isinstance(data, list):
            data = [data]
        typeindex = 0
        for index in range(0, len(data)):
            value = data[index]
            typename = self.type[typeindex]
            size = self.size(typeindex)
            # Incremented independent from outer loop
            # because the type list can vary in size.
            typeindex = (typeindex + 1) % len(self.type)
            if typename == "CHAR":
                if isinstance(value, str):
                    if ord(value) > 255:
                        Logger.write('Replacing multi-byte character "{}" (#{})'.format(value, ord(value)))
                else:
                    Logger.write('Type mismatch: Expect "str" for value "{}"'.format(str(value)))
                continue
            if typename == "STRING":
                if isinstance(value, str):
                    if len(value) > size:
                        Logger.write('String length {} exceeds maximum size of register {} and will be truncated'.format(len(value), size))
                    else:
                        if len(value) == size:
                            Logger.write('No space left to append a zero-terminator to string')
                    # This API assumes that len(string) is equal to the byte length.
                    # Therefore multibyte characters can't be written properly.
                    for p in range(0, len(value)):
                        char = value[p]
                        if ord(char) > 255:
                            Logger.write('Replacing multi-byte character "{}" (#{}) at string index {}'.format(char, ord(char), p))
                else:
                    Logger.write('Type mismatch: Expect type "str" for value "{}"'.format(str(value)))
                continue
            if (typename == "F32") or (typename == "F64"):
                if not isinstance(value, float):
                    Logger.write('Type mismatch: Expect type "float" for value "{}"'.format(str(value)))
                continue
            bits = size * 8
            if typename.startswith("S"):
                if isinstance(value, int):
                    # signed integer type
                    borders = [-(1 << (bits-1)), (1 << (bits-1))-1]
                    if value < borders[0]:
                        Logger.write('Value {} exceeds negative limit of signed type {} range [{}...{}]'.format(value, typename, borders[0], borders[1]))
                    if value > borders[1]:
                        Logger.write('Value {} exceeds positive limit of signed type {} range [{}...{}]'.format(value, typename, borders[0], borders[1]))
                else:
                    Logger.write('Type mismatch: Expect type "int" for value "{}"'.format(str(value)))
            else:
                if isinstance(value, int):
                    # unsigned integer type
                    borders = [0, (1 << bits)-1]
                    if value < borders[0]:
                        Logger.write('Value {} exceeds negative limit of unsigned type {} range [{}...{}]'.format(value, typename, borders[0], borders[1]))
                    if value > borders[1]:
                        Logger.write('Value {} exceeds positive limit of unsigned type {} range [{}...{}]'.format(value, typename, borders[0], borders[1]))
                else:
                    Logger.write('Type mismatch: Expect type "int" for value "{}"'.format(str(value)))
    
    # Crop the specified string at the zero terminator (ASCII-0) value
    # and returns the resulting string.
    # If the string does not contain a zero terminator it is returned unchanged.
    def __crop(self, asciiz):
        if not isinstance(asciiz, str):
            raise TypeError('Expecting a string type')
        last = len(asciiz)
        for index in range(0, len(asciiz)):
            if ord(asciiz[index]) == 0:
                last = index
                break
        return asciiz[0:last]
    
    # Support index syntax: obj[index]
    def __getitem__(self, index):
        if (index < 0) or (index >= len(self.type)):
            raise RuntimeError('Invalid index')
        return self.type[index]
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'type', self.__type,
            'size', self.__size,
            'endian', self.__endian
        )
    
    def __repr__(self):
        return self.__str__()
