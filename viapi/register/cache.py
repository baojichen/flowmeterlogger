from ..utils import *

class Cache:
    # self.__name: str
    # self.__address: int
    # self.__pragma: dict
    
    # Create a new cache object for given register (name + address) and specified cache pragma.
    def __init__(self, name, address, pragma = None):
        if not isinstance(name, str):
            raise TypeError('Invalid register: expecting a string type')
        if not isinstance(address, int):
            raise TypeError('Invalid address: expecting a string type')
        self.__name = name
        self.__address = address
        if pragma is not None:
            if not isinstance(pragma, dict):
                raise TypeError('Invalid cache pragma: expecting a dict type')
            self.__pragma = pragma
        else:
            self.__pragma = {}

    # Returns whether the specified access (e.g. "read" or "write") to the underlying register
    # invalidates the cached values of at least one of the specified registers.
    # If specified register is '*' the function returns True
    # if all cached values are invalidated.
    # Typically this is the case when device is reset
    # since then all volatile register values are lost.
    def invalidates(self, access, reg = None):
        if not isinstance(access, str):
            raise TypeError('Invalid access: expecting a string type')
        if reg is not None:
            # Auto-boxing string to list
            if isinstance(reg, str):
                reg = [reg]
        else:
            reg = ['*']
        result = False
        if access in self.__pragma:
            if 'invalidate' in self.__pragma[access]:
                targets = self.__pragma[access]['invalidate']
                if targets is not None:
                    # Auto-boxing string to list
                    if isinstance(targets, str):
                        targets = [targets]
                    for name in reg:
                        if name in targets:
                            result = True
                            break
        return result
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'name', self.__name,
            'address', self.__address,
            'pragma', self.__pragma
        )
    
    def __repr__(self):
        return self.__str__()
