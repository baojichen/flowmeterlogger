import copy

from ..utils import *

class Array:
    """register array abstraction"""
    # self.__typesize: int
    # self.__count: list (int)
    # self.__element: list (int)
    # self.__padding: list (int)
    # self.__index: int
    # self.__request_offset: list (int)
    # self.__request_count: list (int)
    
    # Typesize is the size (in bytes) of underlying data type (e.g. U8=1, U16=2, U32=4 and so on).
    # Count and element are lists used to define multi-dimensional arrays.
    # Index 0 in count and element always refer to the innermost dimension.
    def __init__(self, typesize, count, element = None):
        if not isinstance(typesize, int):
            raise TypeError('Typesize must be an integer type')
        if typesize <= 0:
            raise ValueError('Typesize must be a positive value')
        # Auto-boxing for count and element
        if count is not None:
            if not isinstance(count, list):
                count = [count]
        else:
            count = [1]
        if element is not None:
            if not isinstance(element, list):
                element = [element]
        else:
            element = [typesize]
        # Check count and element list and append them to equal length
        if len(element) > len(count):
            raise ValueError('Dimension of element size (={}) must be equal or smaller than dimension of count (={})'.format(len(element), len(count)))
        value = typesize
        for index in range(0, len(count)):
            if count[index] <= 0:
                raise ValueError('Invalid negative or zero dimension length: {}'.format(count[index]))
            if index < len(element):
                if element[index] <= 0:
                    raise ValueError('Invalid negative or zero dimension size: {}'.format(element[index]))
                if value > element[index]:
                    raise ValueError('Element size (={}) must be greater or equal than type size (={})'.format(element[index], value))
                # At this point we have element[index] and it increases size
            else:
                element.append(value)
            value = element[index] * count[index]
        # To compute whether we can read/write multiple elements by a single request
        # we need to know if a dimension contain gaps (padding).
        # Since these gaps shall not be read/written we then require multiple requests.
        padding = self.__compute_padding_list(typesize, count, element)
        # Store values
        self.__typesize = typesize
        self.__count = count
        self.__element = element
        self.__padding = padding
        # By default iterate over entire array
        self.__index = 0
        self.__request_offset = [0] * len(self.__count)
        self.__request_count = copy.copy(self.__count)
    
    # Compute the padding list which contains the number of bytes each element in associated dimension is padded.
    # Count and element lists originate from the register definition and must be normalized.
    # If no padding is used the padding list contains zeros only.
    # Index 0 in arguments and return value always refers to the innermost dimension.
    def __compute_padding_list(self, typesize, count, element):
        padding = []
        value = typesize
        # Index 0: innermost dimension
        for index in range(0, len(count)):
            if index < len(element):
                padding.append(element[index] - value)
                value = max(value, element[index])
            else:
                # No more padding once end of element list is reached
                padding.append(0)
            value *= count[index]
        return padding
    
    # Returns the (zero based) byte offset within array.
    # If offset is given it must be a list specifying the position of the element.
    # Omitted elements in offset default to zero.
    # If offset is omitted zero is returned.
    # Index 0 in offset always refers to the innermost dimension.
    def __get_byte_offset(self, offset):
        # To compute the resulting byte address the size of an element (in bytes)
        # in a dimension is multiplied by the offset list piecewise.
        # The results are then added together.
        # This operation is actually the dot product.
        # a) Using numpy: numpy.dot() return numpy.int32 type
        #    int(numpy.dot(offset, self.__element))
        # b) Standalone implementation
        # Do not want to depend on numpy for this single function only
        result = 0
        for index in range(0, len(self.__element)):
            result += offset[index] * self.__element[index]
        return result
    
    # Returns the actual (possibly limited) count of elements to read/write on dimension given by index
    def __get_true_count(self, index, request_offset, request_count):
        return max(0, min(self.__count[index] - request_offset[index], request_count[index]))
    
    # Returns the number of elements (not bytes) on innermost dimension(!) that can be read/written by a single transaction.
    def __get_request_elements(self, request_offset, request_count):
        result = 1
        for index in range(0, len(self.__count)):
            if self.__padding[index] != 0:
                # If padding is non-zero we can read single elements in this dimension only:
                # There is a gap after each element.
                break
            # Padding is zero, so it may be possible to read/write more...
            count = self.__get_true_count(index, request_offset, request_count)
            # a) If offset is non-zero this is a gap in next higher dimension.
            # b) If offset is zero but count does not span all elements
            #    this is also a gap in next higher dimension.
            # In both cases we can't read/write multiple elements in next higher dimension and we can abort.
            # Only if offset is zero and count spans all elements it may be possible to read/write more elements.
            result *= count
            if (request_offset[index] != 0) or (count < self.__count[index]):
                break
        return result
    
    # Adds the number of elements to the given request offset
    # and returns the resulting offset respecting given request count.
    # If the resulting offset exceeds the end of array a StopIteration exception is thrown.
    # Index 0 in input arguments and result refers to the innermost dimension.
    def __offset(self, request_offset, request_count, steps):
        offset = copy.copy(request_offset)
        # Subtile: Can't shortcut steps=0 here because offset or count may be bad
        # which then requires raising a StopIteration() exception.
        # If steps is zero offset does not increase
        carry = steps
        for index in range(0, len(self.__count)):
            count = self.__get_true_count(index, request_offset, request_count)
            # e.g. if offset is pointing to nowhere we receive zero count
            if count <= 0:
                raise StopIteration()
            offset[index] = request_offset[index] + carry % count
            # integer division! (5//3=1, 6//3=2, ...)
            carry = carry // count
            # If carry is zero we're done with offset computation
            if carry == 0:
                break
        # If final carry is not zero we've exceeded end of array
        if carry != 0:
            raise StopIteration()
        return offset
    
    # Normalize (pad + reverse) request offset to same length as count and returns the result.
    # Offset must be a list of integers each integer specifying the zero based offset in associated dimension.
    # If length of offset list is smaller than length of count list unused dimension offsets default to zero.
    # Index zero on input arguments refers to outermost(!) dimension.
    def __normalize_offset(self, request_offset):
        index = len(request_offset)
        while index < len(self.__count):
            request_offset.append(0)
            index = index + 1
        # Index 0: outermost dimension -> innermost dimension
        request_offset.reverse()
        return request_offset
    
    # Checks the specified offset and raises an exception if invalid.
    # Index zero in input argument refers to innermost dimension.
    def __check_offset(self, offset):
        if len(offset) > len(self.__count):
            raise ValueError('Dimension of offset (={}) must not exceed register dimension (={})'.format(len(offset), len(self.__count)))
        for index in range(0, len(offset)):
            if offset[index] < 0:
                raise ValueError('Negative offset value {} not allowed'.format(offset[index]))
        # Each item in offset list must be smaller than associated item in count list.
        # If not that means the item is pointing beyond the array boundary in this dimension.
        for a, z in zip(offset, self.__count):
            if a >= z:
                raise ValueError('Offset exceeds register boundary')
    
    # Normalize (pad + revere) request_count and returns the result.
    # The request_offset must be normalized when calling this function.
    # Index 0 within request_count refers to outermost (!) dimension.
    def __normalize_count(self, request_offset, request_count):
        # First, pad request_count to same length as register dimension.
        # The zeros padded are replaced later.
        length = len(request_count)
        for index in range(length, len(self.__count)):
            request_count.append(0)
        # Innermost dimension first:
        # After that we can uniformly index self.__count and request_count lists.
        request_count.reverse()
        # Default missing elements in request_count to span entire dimension.
        for index in range(0, len(self.__count) - length):
            # offset[i] = 0: request_count[i] default to count[i]
            # offset[i] > 0: request_count is reduced by offset
            request_count[index] = max(0, self.__count[index] - request_offset[index])
        # Limit request_count considering request_offset:
        # E.g. if request_offset[i] exceeds the dimension (it points to nowhere) count is 0
        for index in range(0, len(self.__count)):
            request_count[index] = self.__get_true_count(index, request_offset, request_count)
        return request_count
    
    # Check the specified request count and raises an exception if invalid.
    def __check_count(self, count):
        if len(count) > len(self.__count):
            raise ValueError('Dimension of count (={}) must not exceed register dimension (={})'.format(len(count), len(self.__count)))
        for index in range(0, len(count)):
            if count[index] < 0:
                raise ValueError('Negative count value {} not allowed'.format(count[index]))
    
    # Returns the byte size of an access using given offset and count.
    # First element in offset and count refer to the outermost(!) dimension.
    # The size is limited to the end of the array.
    def iterate(self, request_offset = None, request_count = None):
        # Subtile: Each path should return cloned references only
        #          We dont want to mutate objects in callers scope
        if request_offset is None:
            request_offset = [0] * len(self.__count)
        else:
            if not isinstance(request_offset, list):
                request_offset = [request_offset]
            else:
                request_offset = copy.copy(request_offset)
        if request_count is None:
            # Subtile:
            # a) Clone it, don't want to mutate reference here!
            # b) Index 0 in request_count must be outermost dimension first,
            #    but we clone from innermost dimension first.
            # Can rely on __normalize_count() to reduce this depending on offset.
            request_count = copy.copy(self.__count)
            request_count.reverse()
        else:
            if not isinstance(request_count, list):
                request_count = [request_count]
            else:
                request_count = copy.copy(request_count)
        # Normalization:
        # Index 0 within arguments refers to the outermost dimension!
        # However internally we want index 0 to refer to innermost dimension.
        self.__request_offset = self.__normalize_offset(request_offset)
        self.__check_offset(self.__request_offset)
        self.__request_count = self.__normalize_count(self.__request_offset, request_count)
        self.__check_count(self.__request_count)
        return self
    
    # Support for Python iterators
    def __iter__(self):
        self.__index = 0
        return self
    
    # Support for Python iterators
    def __next__(self):
        offset = self.__offset(self.__request_offset, self.__request_count, self.__index)
        count = self.__get_request_elements(self.__request_offset, self.__request_count)
        self.__index += count
        # list: [offset (bytes), number of elements, size of elements (bytes)]
        return [self.__get_byte_offset(offset), count, count * self.__typesize]
    
    # Create a readable string representation
    def __str__(self):
        return stringify(
            'typesize', self.__typesize,
            'count', self.__count,
            'element', self.__element,
            'padding', self.__padding,
            'index', self.__index,
            'request_offset', self.__request_offset,
            'request_count', self.__request_count
        )
    
    def __repr__(self):
        return self.__str__()
