import os
import sys
import urllib.parse
import packaging.version
import pkg_resources

from .utils import *

class Updater:
    """updater"""
    MODULE = 'viapi'
    
    # Updates the given module hosted in the repository at the given URL.
    # By default if module argument is omitted it update VIAPI to the latest version.
    # Without URL argument assume the module is at the known repository location.
    @staticmethod
    def upgrade(url = None, module = None):
        # If we reside in a frozen application we can't upgrade packages this way
        if not getattr(sys, 'frozen', False):
            from .db import DB
            if url is None:
                url = DB.REPOSITORY
            if len(url) > 0:
                host = urllib.parse.urlparse(url).netloc
                repo = '--trusted-host {} --index-url {}'.format(host, url)
            else:
                repo = ''
            if module is None:
                module = Updater.MODULE
            Logger.write('Upgrading {}'.format(module))
            cmd = '"' + sys.executable + '" -m pip install --upgrade {} {}'.format(repo, module)
            Logger.write('EXEC: ' + cmd)
            os.system(cmd)
    
    # Check if the specified API version can be processed by this library.
    # If so the function returns True, otherwise the library requires an update and the function returns False.
    @staticmethod
    def supports(version):
        # None means no particular VIAPI version is needed
        if version is None:
            return True
        result = False
        if isinstance(version, str):
            version = packaging.version.parse(version)
        # Obtain our version from info given in setup.py here.
        # If we can't find that info assume we run a local developer install
        try:
            ours = packaging.version.parse(Updater.version())
            if version <= ours:
                result = True
        except:
            result = True
        return result
    
    # Returns the version of this library
    @staticmethod
    def version():
        return pkg_resources.get_distribution(Updater.MODULE).version
    
    # Fetches register database from web into local site-packages directory
    @staticmethod
    def update_database():
        from .db import DB
        # If we reside in a frozen application we can't upgrade packages this way
        if not getattr(sys, 'frozen', False):
            Downloader.set_target_directory(None)
            # e.g. https://api.voegtlin.com/db/
            base = DB.REPOSITORY + '/' + DB.SUBDIR
            url = base + '/' + DB.SUBDIR + '.zip'
            Logger.write('Downloading database')
            result = Downloader.get(url, None, base)
            Logger.write('Updated {}'.format(result))
