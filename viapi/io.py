import os
import platform
import serial
import minimalmodbus

from .utils import *

class IO:
    """Serial IO channel"""
    # self.__port: str
    # self.__slave: int
    # self.__channel: minimalmodbus.Instrument

    # Returns a list of serial ports available in the system.
    # If no serial ports are available the function returns an empty list.
    # Each serial port returned is a string representation of the device
    # that can be used to create IO objects.
    @staticmethod
    def ports(available_ports_only = False):
        result = []
        try:
            import serial.tools.list_ports
            for port in serial.tools.list_ports.comports():
                # There have been some changes behind the scenes.
                # It seems former version directly return the device name.
                if isinstance(port, serial.tools.list_ports_common.ListPortInfo):
                    result.append(str(port.device))
                else:
                    result.append(str(port))
        except:
            # If we fail to query serial ports somehow we return an empty list.
            try:
                if os.name == 'nt':
                    # On Windows we try to read serial ports from registry.
                    import winreg
                    with winreg.ConnectRegistry(None, winreg.HKEY_LOCAL_MACHINE) as registry:
                        with winreg.OpenKey(registry, r'HARDWARE\DEVICEMAP\SERIALCOMM') as key:
                            try:
                                index = 0
                                while True:
                                    # result: key, value, type
                                    # This finally raises an exception if no more values are available
                                    result.append(str(winreg.EnumValue(key, index)[1]))
                                    index += 1
                            except:
                                pass
                else:
                    # Not a Windows system: Assume Linux and query /dev folder
                    import glob
                    for path in ['/dev/ttyS*', '/dev/ttyUSB*', '/dev/ttyACM*']:
                        for device in glob.glob(path):
                            result.append(device)
            except:
                pass
        if available_ports_only:
            available_ports = []
            for port in result:
                # We try to open a port first to check if port is also currently free.
                try:
                    s = serial.Serial(port)
                    s.close()
                except (OSError, serial.SerialException):
                    # If we see an exception we skip this port.
                    continue
                available_ports.append(port)
            result = available_ports
        return result

    # Create a new serial IO communication channel
    def __init__(self, port, slave = 247, baudrate = 9600, bits = serial.EIGHTBITS, parity = serial.PARITY_NONE, stops = serial.STOPBITS_ONE):
        if (slave < 0) or (slave > 247):
            raise ValueError('Invalid MODBUS slave address')
        if bits not in [serial.FIVEBITS, serial.SIXBITS, serial.SEVENBITS, serial.EIGHTBITS]:
            raise ValueError('Invalid serial configuration: bad number of data bits')
        if not isinstance(parity, str):
            raise ValueError('Invalid serial configuration: bad parity')
        parity = parity[0].upper()
        if parity not in [serial.PARITY_NONE, serial.PARITY_EVEN, serial.PARITY_ODD, serial.PARITY_MARK, serial.PARITY_SPACE]:
            raise ValueError('Invalid serial configuration: bad parity')
        if stops not in [serial.STOPBITS_ONE, serial.STOPBITS_ONE_POINT_FIVE, serial.STOPBITS_TWO]:
            raise ValueError('Invalid serial configuration: bad number of stop bits')
        port = self.__qualify_port(port)
        # Create and open a new serial communication channel.
        # Notes:
        # - It seems (for some library versions?) it's required to set baudrate after creating the object.
        # - minimalmodbus is sharing serial objects referring to the same serial port internally
        channel = minimalmodbus.Instrument(port, slave, mode = 'rtu', debug = False)
        # Warning: When multiple objects share a serial communication channel
        # this reconfiguration affects all objects which may be unexpected.
        settings = {
            'baudrate': baudrate,
            'parity': parity,
            'bytesize': bits,
            'stopbits': stops,
            'xonxoff': False,
            'dsrdtr': False,
            'timeout': 2
        }
        channel.serial.apply_settings(settings)
        # Subtile: properties are not available before that
        self.__port = port
        self.__slave = slave
        self.__channel = channel

    # Close serial port when object is destroyed
    def __del__(self):
        self.close()

    # Close serial port
    def close(self):
        # Partial created objects may not yet contain __channel attribute
        try:
            if self.__channel is not None:
                try:
                    if self.__channel.serial.is_open:
                        Logger.write('Closing serial port {}'.format(self.__port))
                        # It seems minimalmodbus is re-open this automatically
                        # if serial port is still in use by other instances
                        self.__channel.serial.close()
                finally:
                    self.__channel = None
        except AttributeError:
            pass
    
    # Reads given number of registers (not bytes) via MODBUS at the specified address.
    # The result is returned as a list of integers even if only a single value is requested.
    # The function issues multiple MODBUS requests and combines their results
    # if the requested amount of registers exceeds the limits specified by the MODBUS standard.
    # In case of an error while reading the register the function raises an exception.
    def read(self, address, count):
        if (address < 0) or (address > 0xFFFF):
            raise ValueError('Invalid MODBUS register address')
        if count < 0:
            raise ValueError('Unable to read a negative number of registers')
        if self.slave == 0:
            raise RuntimeError('Unable to read registers from MODBUS broadcast address')
        result = []
        if count > 0:
            index = 0
            try:
                data = []
                if count > 1:
                    while count > 0:
                        # MODBUS allows to read up to 125 register with each call.
                        # However to prevent misalignment for 32 and 64 bit
                        # we read a maximum of 120 registers.
                        # If reading more registers than that we need to loop and issue multiple calls.
                        block = min(120, count)
                        Logger.write('READ @ {} N={}'.format(hex(address + index), block))
                        # Use concat not append here: This function always returns a list of values
                        data += self.channel.read_registers(address + index, block)
                        count = count - block
                        index = index + block
                else:
                    Logger.write('READ @ {} N=1'.format(hex(address + index)))
                    # This function always returns a scalar (e.g. int)
                    data.append(self.channel.read_register(address))
                result = data
            except:
                raise RuntimeError('I/O error while reading MODBUS @ {}'.format(hex(address + index)))
        return result

    # Writes the specified values via MODBUS to the given address.
    # On success the number of values (not size in bytes) is returned.
    # The function issues multiple MODBUS requests if the requested amount of registers
    # exceeds the limits specified by the MODBUS standard.
    # In case of an error while writing the function raises an exception.
    def write(self, address, data):
        result = 0
        if (address < 0) or (address > 0xFFFF):
            raise ValueError('Invalid MODBUS register address')
        count = len(data)
        if count > 0:
            index = 0
            try:
                if count > 1:
                    while count > 0:
                        # MODBUS allows to write up to 123 registers with each call.
                        # However to prevent misalignment for 32 and 64 bit
                        # we write a maximum of 120 registers.
                        # If writing more registers than that we need to loop and issue multiple calls.
                        block = min(120, count)
                        Logger.write('WRITE @ {}'.format(hex(address + index)), data[index:index+block])
                        # This function does not return a value instead it raises exceptions on error
                        self.channel.write_registers(address + index, data[index:index+block])
                        count = count - block
                        index = index + block
                else:
                    Logger.write('WRITE @ {}'.format(hex(address)), data[0])
                    # This function does not return a value instead it raises exceptions on error
                    self.channel.write_register(address, data[0])
                result = len(data)
            except:
                raise RuntimeError('I/O error while writing MODBUS @ {}'.format(hex(address + index)))
        return result

    # Returns the MODBUS slave address
    @property
    def slave(self):
        return self.__slave

    # Returns the serial port in use
    @property
    def port(self):
        return self.__port

    # Returns the serial communication channel associated with this object
    @property
    def channel(self):
        return self.__channel

    # Fully qualify specified port name and return the result
    def __qualify_port(self, port):
        # The value of port can be int or string type:
        # a) If string type we expect it to be the serial port name to open.
        # b) If int type we expect it to be the serial port number to open.
        #    The actual serial port name then depends on the underlying operating system.
        # c) An IP address for MODBUS over TCP
        os_name = platform.system().lower()
        if os_name == 'windows':
            prefix = '\\\\.\\'
            if isinstance(port, int):
                if port <= 0:
                    raise ValueError('Invalid serial port number')
                # If a simple int x is given that is expanded to \\.\COMx here
                port = '{}{}'.format(prefix + 'COM', port)
            else:
                port.upper()
                # Anything else but not an IP address.
                # For now we assume it's a serial port name.
                if not port.startswith(prefix):
                    # Assume port is string e.g. COM3 which we expand to \\.\COM3 here.
                    # Serial ports on Windows must be prefixed with \\.\ if port number is higher than 9.
                    port = '{}{}'.format(prefix, port)
        else:
            if isinstance(port, int):
                if port <= 0:
                    raise ValueError('Invalid serial port number')
                # Typical serial port name examples:
                # Linux: /dev/ttyS*
                #        /dev/ttypACM*
                # MacOS: /dev/tty.usbserial-*
                #        /dev/tty.usbmodem*
                #        /dev/tty.PL2303-*
                portname = {'linux': '/dev/ttyS', 'darwin': '/dev/tty.usbmodem'}
                if not os_name in portname:
                    raise RuntimeError('Unable to determine serial port name for your OS')
                port = '{}{}'.format(portname.get(os_name), port)
            else:
                if not port.startswith('/dev/'):
                    # The argument is not a fully qualified serial port name (e.g. 'ttyS0').
                    # Note that we don't change fully qualified names.
                    port = '/dev/' + port
        return port

    # Create a readable string representation
    def __str__(self):
        return stringify(
            'port', self.__port,
            'slave', self.__slave,
            'baudrate', self.__channel.serial.baudrate,
            'bytesize', self.__channel.serial.bytesize,
            'parity', self.__channel.serial.parity,
            'stopbits', self.__channel.serial.stopbits
        )

    def __repr__(self):
        return self.__str__()
