from operator import contains
import viapi
import os
import time
import csv
import threading
import numpy as np
import atexit
import sys
import signal
com_port_no=9
#logPath:str=os.path.realpath("logs")

logPath:str=r"D:\HLab\Lab data\FlowmetersLog"
viapi.utils.Logger.redirect(None)
serial = viapi.IO(port=com_port_no, slave=1, baudrate=9600, parity='N', stops=2)
print('Serial port opened')
viapi.Instrument.REPOSITORY = os.path.realpath("db.zip")
device = viapi.Instrument()
device.connect(serial)
print('Dewar connected')
serial2 = viapi.IO(port=com_port_no, slave=2, baudrate=9600, parity='N', stops=2)
device2 = viapi.Instrument()
device2.connect(serial2)
print('Tip connected')
serial3 = viapi.IO(port=com_port_no, slave=3, baudrate=9600, parity='N', stops=2)
device3 = viapi.Instrument()
device3.connect(serial3)
print('Shield connected')
headerPrinted:bool=False
exitFlag:bool=False
setFlag:bool=False  
endFlag:bool=False
t0=time.gmtime()
time1=time.strftime('%Y/%m/%d', t0)+" 00:00:00 UTC"
dayStart=time.strptime(time1,'%Y/%m/%d %H:%M:%S %Z')
fname = logPath+'\\'+time.strftime('%d_%m_%Y_%H_%M_%S', t0)+'.csv'
csvFile=open(fname, 'w', newline='')
fnameSetting = logPath+'\\'+time.strftime('%d_%m_%Y_Setting', t0)+'.csv'
csvSetting=None
writerSetting=None
fieldnames = [
        'DateTime',
        'DateTimeSec',
        'Dewar_PressureSetPoint',
        'Dewar_P',
        'Dewar_I',
        'Dewar_D',
        'Tip_FlowSetPoint',
        'Tip_P',
        'Tip_I',
        'Tip_D',
        'Tip_N',
        'Shield_FlowSetPoint',
        'Shield_P',
        'Shield_I',
        'Shield_D',
        'Shield_N'
]
if(os.path.exists(fnameSetting)):
    csvSetting=open(fnameSetting, 'a', newline='')
    writerSetting = csv.DictWriter(csvSetting, fieldnames=fieldnames)
else:
    csvSetting=open(fnameSetting, 'w', newline='')
    writerSetting = csv.DictWriter(csvSetting, fieldnames=fieldnames)
    writerSetting.writeheader()
    
def LogSetting():
    t2=time.gmtime()
    t3=time.time()
    dic1:dict={
        'DateTime':time.strftime('%H:%M:%S %d/%m/%Y', t2),
        'DateTimeSec':str(float(time.mktime(t2)-time.mktime(dayStart))+(t3-float(int(t3)))),
        'Dewar_PressureSetPoint':device.read('PRESSURE_SETPOINT'),
        'Dewar_P':device.read("PRESSURE_CONTROL_PARAM_P"),
        'Dewar_I':device.read("PRESSURE_CONTROL_PARAM_I"),
        'Dewar_D':device.read("PRESSURE_CONTROL_PARAM_D"),
        'Tip_FlowSetPoint':device2.read('SETPOINT'),
        'Tip_P':device2["CONTROL_PARAM_P"],
        'Tip_I':device2["CONTROL_PARAM_I"],
        'Tip_D':device2["CONTROL_PARAM_D"],
        'Tip_N':device2["CONTROL_PARAM_N"],
        'Shield_FlowSetPoint':device3.read('SETPOINT'),
        'Shield_P':device3["CONTROL_PARAM_P"],
        'Shield_I':device3["CONTROL_PARAM_I"],
        'Shield_D':device3["CONTROL_PARAM_D"],
        'Shield_N':device3["CONTROL_PARAM_N"]
    }
    writerSetting.writerow(dic1)
    csvSetting.flush()
    
def main():
    
    
    def Logging():
      global headerPrinted
      while True:
        if(endFlag):
            break
        if(exitFlag):
            0
            #print("")
            #print('file saved. press Enter to start logging, or you can close the logger...')
            #print('or if you want to set parameters, input "set"/"s" then press Enter...')
        while True:
            if not exitFlag:
                break
            time.sleep(0.5)
        if(not endFlag):
            print('logging started. press Enter to pause the logging...')
        fieldnames = [
            'DateTime',
            'DateTimeSec',
            'Dewar_CurrentPressure',
            'Dewar_CurrentFlow',
            #'Dewar_FlowSetPoint',
            #'Dewar_Temperature',
            #'Dewar_Totalisator',
            #'Dewar_ValveLoad',
            #'Dewar_PressureSetPoint',
            'Tip_CurrentFlow',
            #'Tip_FlowSetPoint',
            #'Tip_Temperature',
            #'Tip_Totalisator',
            #'Tip_ValveLoad',
            'Shield_CurrentFlow',
            #'Shield_FlowSetPoint',
            #'Shield_Temperature',
            #'Shield_Totalisator',
            #'Shield_ValveLoad'
        ]
        writer = csv.DictWriter(csvFile, fieldnames=fieldnames)
        if(not headerPrinted):
            headerPrinted=True
            writer.writeheader()
        while True:
            if(endFlag):
                break
            if(exitFlag):
                #csvFile.close()
                break
            t2=time.gmtime()
            t3=time.time()
            dic:dict={
                'DateTime':time.strftime('%H:%M:%S %d/%m/%Y', t2),
                'DateTimeSec':str(float(time.mktime(t2)-time.mktime(dayStart))+(t3-float(int(t3)))),
                'Dewar_CurrentPressure':device.read('PRESSURE_VALUE'),
                'Dewar_CurrentFlow':device.read('FLOW_VALUE'),
                #'Dewar_FlowSetPoint':device.read('SETPOINT'),
                #'Dewar_Temperature':device.read('TEMPERATURE_VALUE'),
                #'Dewar_Totalisator':device.read('TOTALIZER_VALUE'),
                #'Dewar_ValveLoad':device.read('VALVE_LOAD'),
                #'Dewar_PressureSetPoint':device.read('PRESSURE_SETPOINT'),
                'Tip_CurrentFlow':device2.read('FLOW_VALUE'),
                #'Tip_FlowSetPoint':device2.read('SETPOINT'),
                #'Tip_Temperature':device2.read('TEMPERATURE_VALUE'),
                #'Tip_Totalisator':device2.read('TOTALIZER_VALUE'),
                #'Tip_ValveLoad':device2.read('VALVE_LOAD'),
                'Shield_CurrentFlow':device3.read('FLOW_VALUE'),
                #'Shield_FlowSetPoint':device3.read('SETPOINT'),
                #'Shield_Temperature':device3.read('TEMPERATURE_VALUE'),
                #'Shield_Totalisator':device3.read('TOTALIZER_VALUE'),
                #'Shield_ValveLoad':device3.read('VALVE_LOAD')
            }
            writer.writerow(dic)
            csvFile.flush()
            #print('press Enter to pause logging...')
            #print()
        
        
            #dayStart.tm_year=t0.tm_year
            #dayStart.tm_mon=t0.tm_mon
            #dayStart.tm_mday=t0.tm_mday
            #device2.raw.write(16463,0,[1,0])
            #time.strftime('%H:%M:%S', time.gmtime())
            print("  "+time.strftime('%H:%M:%S', time.gmtime())+
                    '        '+#str(device2.raw.read(16463,0,2))+
                    #str((time.time()-float(int(time.time()))))+
                    #str(np.round(float(time.mktime(t2)-time.mktime(dayStart))+0,1))+
                    str(np.round(float(time.mktime(t2)-time.mktime(dayStart))+(t3-float(int(t3))),1))+
                    '        ShieldFlow:'+
                    str(np.round(dic['Shield_CurrentFlow'],4))+
                    '        TipFlow:'+
                    str(np.round(dic['Tip_CurrentFlow'],4))+
                    '        DewarPres: '+
                    str(np.round(dic['Dewar_CurrentPressure'],4)),end='\r'         )
        
     
    def Setting():
        global setFlag
        global exitFlag
        global endFlag
        while True:
            while True:
                if setFlag:
                    break
                time.sleep(0.5)
            print("===========you are in setting mode.===========")
            print("Here is examples to set parameters to the three gas meter:")
            print("1. to set dewar pressure to 0.3 bar, input \"d 0.3\" then press Enter")
            print("2. to set tip flow to 6.5 L/min, input \"t 6.5\" then press Enter")
            print("3. to set shield flow to 3.8 L/min, input \"s 3.8\" then press Enter")
            print("4. to set PID parameters, press \"p\" then Enter...")
            print("5. to leave without change then restart logging, press Enter...")
            print("6. to close the logger and the gas meters safely, press \"c\" then Enter...")
            userInput:str=input("")
            if(userInput==""):
                setFlag=False
                #print("")
                #print('press Enter to start logging, or you can close the logger...')
                #print('or if you want to set parameters, input "set"/"s" then press Enter...')
                exitFlag=False
                continue
            if(userInput=="c"):
                #setFlag=False
                endFlag=True
                exitFlag=False
                break
            if(userInput=="p"):    
                #pid: 0-10000
                #n:0-8000
                #
                print("============PID setting============")
                print("    Current states:")
                print("    dewar: P="+
                      str(device["PRESSURE_CONTROL_PARAM_P"])+
                      "    I="+
                      str(device["PRESSURE_CONTROL_PARAM_I"])+
                      "    D="+
                      str(device["PRESSURE_CONTROL_PARAM_D"]))
                print("    tip: P="+
                      str(device2["CONTROL_PARAM_P"])+
                      "    I="+
                      str(device2["CONTROL_PARAM_I"])+
                      "    D="+
                      str(device2["CONTROL_PARAM_D"])+
                      "    N="+
                      str(device2["CONTROL_PARAM_N"]))
                print("    shield: P="+
                      str(device3["CONTROL_PARAM_P"])+
                      "    I="+
                      str(device3["CONTROL_PARAM_I"])+
                      "    D="+
                      str(device3["CONTROL_PARAM_D"])+
                      "    N="+
                      str(device3["CONTROL_PARAM_N"]))
                print()
                print("    use \"d\" to control dewar, \"t\" for tip, \"s\" for shield")
                print("    use \"p\" \"i\" \"d\" \"n\" to control P, I, D, N parameter")
                print("    for example, to change the tip flow's P parameter to 200,")
                print("    then input \"tp 200\" then Enter...")
                print("    or press Enter to continue without any change...")
                while True:
                    try:
                        cmd1=input("    ")
                        if(cmd1==""):
                            break
                        cmds1=cmd1.split(' ')
                        if(not(len(cmds1)==2)):
                            raise
                        cmds2=cmds1[0]#.split('')
                        if(not(len(cmds2)==2)):
                            raise
                        val=float(cmds1[1])
                        if(val>10000 or val<0):
                            raise
                        if(cmds2[0]=="d"):
                            if(cmds2[1]=="p"):
                                device["PRESSURE_CONTROL_PARAM_P"]=val
                                print("    success. now P param for dewar is "+str(device["PRESSURE_CONTROL_PARAM_P"]))
                            elif(cmds2[1]=="i"):
                                device["PRESSURE_CONTROL_PARAM_I"]=val
                                print("    success. now I param for dewar is "+str(device["PRESSURE_CONTROL_PARAM_I"]))
                            elif(cmds2[1]=="d"):
                                device["PRESSURE_CONTROL_PARAM_D"]=val
                                print("    success. now D param for dewar is "+str(device["PRESSURE_CONTROL_PARAM_D"]))
                            else:
                                raise
                        elif(cmds2[0]=="t"):
                            if(cmds2[1]=="p"):
                                device2["CONTROL_PARAM_P"]=val
                                print("    success. now P param for tip is "+str(device2["CONTROL_PARAM_P"]))
                            elif(cmds2[1]=="i"):
                                device2["CONTROL_PARAM_I"]=val
                                print("    success. now I param for tip is "+str(device2["CONTROL_PARAM_I"]))
                            elif(cmds2[1]=="d"):
                                device2["CONTROL_PARAM_D"]=int(val)
                                print("    success. now D param for tip is "+str(device2["CONTROL_PARAM_D"]))
                            elif(cmds2[1]=="n"):
                                device2["CONTROL_PARAM_N"]=int(val)
                                print("    success. now N param for tip is "+str(device2["CONTROL_PARAM_N"]))
                            else:
                                raise
                        elif(cmds2[0]=="s"):
                            if(cmds2[1]=="p"):
                                device3["CONTROL_PARAM_P"]=val
                                print("    success. now P param for shield is "+str(device3["CONTROL_PARAM_P"]))
                            elif(cmds2[1]=="i"):
                                device3["CONTROL_PARAM_I"]=val
                                print("    success. now I param for shield is "+str(device3["CONTROL_PARAM_I"]))
                            elif(cmds2[1]=="d"):
                                device3["CONTROL_PARAM_D"]=int(val)
                                print("    success. now D param for shield is "+str(device3["CONTROL_PARAM_D"]))
                            elif(cmds2[1]=="n"):
                                if(val>8000):
                                    raise
                                device3["CONTROL_PARAM_N"]=int(val)
                                print("    success. now N param for shield is "+str(device3["CONTROL_PARAM_N"]))
                            else:
                                raise
                        else:
                            raise
                        LogSetting()
                        break
                    except:
                        print("    invalid command! please retry.")
                    
                setFlag=False
                exitFlag=False
                continue 
            cmds:list=userInput.split(' ')
            try:
                if((not(len(cmds) == 2)) or not(["d","t","s"].__contains__(cmds[0]))):
                    raise
                elif(cmds[0]=="d"):
                    param:float=float(cmds[1])
                    if(param<0 or param>1):
                        raise
                    print(param)
                    device["PRESSURE_SETPOINT"]=param
                    time.sleep(0.2)
                    ret=device.read('PRESSURE_SETPOINT')
                    print("success. Now the dewar pressure is set to "+str(ret)+" bar")
                elif(cmds[0]=="t"):
                    param:float=float(cmds[1])
                    if(param<0 or param>20):
                        raise
                    print(param)
                    device2["SETPOINT"]=param
                    time.sleep(0.2)
                    ret=device2.read('SETPOINT')
                    print("success. Now the tip flow is set to "+str(ret)+" L/min")
                elif(cmds[0]=="s"):
                    param:float=float(cmds[1])
                    if(param<0 or param>20):
                        raise
                    device3["SETPOINT"]=param
                    time.sleep(0.2)
                    ret=device3.read('SETPOINT')
                    print("success. Now the shield flow is set to "+str(ret)+" L/min")
                LogSetting()
                setFlag=False
                #print()
                #print('press Enter to start logging, or you can close the logger...')
                #print('or if you want to set parameters, input "set" then press Enter...')
                exitFlag=False
                continue    
            except:
                print("===invalid command! Try to input command again.===")
            
                continue
           

    
    
    def Control():
        global exitFlag
        global setFlag
        while True:
            if(not setFlag):
                userInput:str=input("")
                if(userInput==""):
                    exitFlag=not exitFlag
                time.sleep(0.6)
                if( exitFlag==True):
                    setFlag=True
            else:
                time.sleep(0.1)
                if(endFlag):
                    break
    

    
    
    
    t1=threading.Thread(target=Logging)
    t2=threading.Thread(target=Control)
    t3=threading.Thread(target=Setting)
    t1.start()
    t2.start()
    t3.start()
    t1.join()
@atexit.register
def Finalize():
    device["PRESSURE_SETPOINT"]=0
    device2["SETPOINT"]=0
    device3["SETPOINT"]=0
    LogSetting()
    csvFile.close()
    csvSetting.close()
def kill_handler(*args):
    #Finalize()
    sys.exit(0)
signal.signal(signal.SIGINT, kill_handler)
signal.signal(signal.SIGTERM, kill_handler)
signal.signal(signal.SIGBREAK, kill_handler)
try:
    main()
finally:
    sys.exit(0)
    Finalize()

